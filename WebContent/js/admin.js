(function($) { 
	$.koorz = $.koorz || {
        version: "v1.0.0"
    };
    $.extend($.koorz, {});
})(jQuery); 
/**
 * 文件验证
 */
(function($){
	$.koorz.file={
			conf:{
				pic:"jpg,jpeg,png,gif"
			},
			pic_validate:function(filename)
			{
				var point=filename.lastIndexOf(".")+1;
				var type=filename.substr(point);
				if($.koorz.file.conf.pic.match(type)!=null)
					{
						return true;
					}
				else{
						return false;
				}
			},
	}
})(jQuery);
/**
 * 加载页面
 */

(function($) {
	$.koorz.loadpage = {
		conf: {
			id:"",
			url:"",
			ajaxData:{},
            successCall:"",
            success:false,
		},
		load: function(){
			var self = $.koorz.loadpage;
			if($(self.conf.id)[0]){
				var container = $(self.conf.id);
			   	$.post(self.conf.url,self.conf.ajaxData,function(resp) {
			   		if(resp){
			   			if(resp.code==0)
			   				{
			   					alert(resp.msg);
			   				}
			   			else
			   				{
			   				if(self.conf.successCall!="")
			   					{
			   						alert(self.conf.successCall);
			   					}
			   				container.empty();
				   			container.append(resp);
				   		    self.conf.success=true;
			   				}
			   		}
			    });
    		}
		}
	};
})(jQuery); 
/**
 * 初始化菜单
 */

(function($) {
	$.koorz.menu = {
		conf: {
			id:"#navi"
		},
		init:function(){
		   var self = $.koorz.menu;
		   if($(self.conf.id)[0]){
			   var navi = $(self.conf.id);
			   navi.find("h3[menu]").each(function(i,ele){
				  var menu = $(this);
				  menu.hover(function(){
						$(this).addClass("focus");
					},function(){
						$(this).removeClass("focus");
					});
				  $("#"+menu.attr('menu')).toggle("slow");
				  menu.click(function(){
					  $(".navi_hover").removeClass("navi_hover");
					  $(this).addClass("navi_hover");
					  $("#"+menu.attr('menu')).toggle("slow");
				  });
				  $("#"+menu.attr('menu')).find("li").each(function(i,ele){
					    var item=$(this);
						item.hover(function(){
							$(this).addClass("focus");
						},function(){
							$(this).removeClass("focus");
						});
						item.click(function(e) {
							$(".selecte_item").removeClass("selecte_item");
							  $(this).addClass("selecte_item");
							var loadpage = $.koorz.loadpage;
							loadpage.conf.url=$(this).attr("url");
							if(menu.attr('menu').match("crawl")!=null){
								if($(this).attr("url").match("/admin/crawl/index")){
									loadpage.conf.id = "#container";
									loadpage.conf.ajaxData={page:1};
								}else{
									alert("非法路径");
								}	
							}else if(menu.attr('menu').match("category")!=null){
								if($(this).attr("url").match("/admin/Category/Category")){
									loadpage.conf.id = "#container";
									loadpage.conf.ajaxData={page:1};
								}else{
									alert("非法路径");
								}	
							}else if(menu.attr('menu').match("user")!=null){
								if($(this).attr("url").match("/admin/User/index")){
									loadpage.conf.id = "#container";
									loadpage.conf.ajaxData={page:1};
								}else{
									alert("非法路径");
								}	
							}else if(menu.attr('menu').match("editor")!=null){
								alert("editor");
							}else if(menu.attr('menu').match("role")!=null){
								if($(this).attr("url").match("/admin/Role/index")){
									loadpage.conf.id = "#container";
								}else{
									alert("非法路径");
								}	
							}else if(menu.attr('menu').match("similar_items")!=null){
								if($(this).attr("url").match("/admin/SimilarItems/index")){
									loadpage.conf.id = "#container";
									loadpage.conf.ajaxData={page:1};
								}else{
									alert("非法路径");
								}	
							}else if(menu.attr('menu').match("front")!=null){
								alert("front");
							}else if(menu.attr('menu').match("spread")!=null){
								alert("spread");
							}else if(menu.attr('menu').match("taobao")!=null){
								alert("taobao");
							}else if(menu.attr('menu').match("suggest")!=null){
								alert("suggest");
							}else{
								alert("非法路径");
							}
							loadpage.load();
						});
				  });
			   });
		   }
		}			
	};
})(jQuery);


/**
 * 点击分类，显示该分类下的标签
 **/
(function($) {
	$.koorz.showLables = {
			conf:{
				id:"#labels",
				list:".category .list",
				url:"/admin/Category/CategotyLabel",
			},
			search:function(){
				var self = $.koorz.showLables;
				var list = $(self.conf.list);
				list.find("li").each(function(i,ele){
					var item = $(this);
					item.click(function(){
						$(".container .biglabels .LabelsFalls").show();
						$(".selected").removeClass("selected");
						$(this).addClass("selected");
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id=self.conf.id;
						loadpage.conf.url=self.conf.url;
						loadpage.conf.ajaxData = {page:1,id:$(this).attr("value")};
						loadpage.load();
					});
				});
				
			}
	}
})(jQuery);
/**
 * 新建分类
 */
(function($) {
	$.koorz.addCategory = {
			conf:{
				id:".container .categoryContent .category #createCategory",
				url:"/admin/Category/addCategory",
			},
			create:function(){
				var self = $.koorz.addCategory;
				$(self.conf.id).click(function(){
					var html = '<div class="opacity">';
					html += '<div class="addCategoryName">';
					html+='<span>分类名称</span><input type="text" id="categoryName">';
					html+='<span id="confirm">确认</span><span id="cancel">取消</span></div></div>';
					$("#container").append(html);
					$(".container .addCategoryName #confirm").click(function(){
						if($(".container .addCategoryName #categoryName").attr("value") == ""){
							alert("分类名称不可为空！");
						}else{
							$.get(self.conf.url,{name:$(".container .addCategoryName #categoryName").attr("value")},function(resp){
								$(".opacity").remove();
								$(".addCategoryName").remove();
								if(resp.code == 1){
									setTimeout(alert(resp.msg),1000);
								}else{
									setTimeout(alert(resp.msg),1000);
								}
							});
						}
						
					});
					$(".container .addCategoryName #cancel").click(function(){
						$(".opacity").remove();
						$(".addCategoryName").remove();
					});
				});
				
			}
	}
})(jQuery);
/**
 * 删除分类
 */
(function($) {
	$.koorz.delCategory = {
			conf:{
				id:".container .categoryContent .category #deleteCategory",
				url:"/admin/Category/delCategory"
			},
			del:function(){
			    var self = $.koorz.delCategory;
			    $(self.conf.id).click(function(){
			    	var num = 0;
			    	var item = "";
				    $(".category .list").find("li").each(function(i,ele){
				    	var thiz = $(this);
				    	if(thiz.hasClass("selected")){
				    		num ++;
				    		item = thiz.attr("value");
				    	}
				    });
				    if(num == 0 && item == ""){
				    	alert("请先选择一个分类！");
				    }else{
				    	var html = '<div class="opacity">';
				        html += '<div class="delConfirm">确认删除该分类？';
				    	html+='<span id="yes">yes</span><span id="no">no</span></div></div>';
				    	$("#container").append(html);
				    	$(".container  #yes").click(function(){
				    		$.get(self.conf.url,{id:item},function(resp){
								$(".opacity").remove();
								$(".addCategoryName").remove();
								if(resp.code == 1){
									setTimeout(alert(resp.msg),1000);
								}else{
									setTimeout(alert(resp.msg),1000);
								}
							});
				    	});
				    	$(".container  #no").click(function(){
				    		$(".opacity").remove();
							$(".addCategoryName").remove();
				    	});
				    }
			    });
			    
			}
	}
})(jQuery);
/**
 * 分类上下页
 */
(function($) {
	$.koorz.categoryPrePage = {
			conf:{
				preid:".container .categoryContent .category #prePage",
				nextid:".container .categoryContent .category #nextPage",
				url:"/admin/Category/Category"
			},
			pre:function(){
				var self = $.koorz.categoryPrePage;
				$(self.conf.preid).click(function(){
					if($(self.conf.preid).attr("value") == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#container";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {page:--num};
						loadpage.load();
					}
				});
				var prepage = $(self.conf.preid).attr("value");
				var nextpage = $(self.conf.nextid).attr("value");
				$(self.conf.nextid).click(function(){
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#container";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {page:++num};
						loadpage.load();
					}
				});
			}
	}
})(jQuery);
/**
 * 删除某分类下的标签
 */
(function($) {
	$.koorz.delLabels = {
			conf:{
				id:".container .categoryContent  .labels .list",
				url:"/admin/Category/delLabel"
			},
			delLabels:function(){
				var self = $.koorz.delLabels;
				$(self.conf.id).find(".item").each(function(){
					var thiz = $(this);
					thiz.find("a").click(function(){
						$(".del_item").removeClass("del_item");
						thiz.addClass("del_item");
						var html = '<div class="opacity">';
				        html += '<div class="delConfirm">确认删除该标签？';
				    	html+='<span id="yes">yes</span><span id="no">no</span></div></div>';
				    	$("#container").append(html);
				    	$(".container #yes").click(function(){
				    		$.get(self.conf.url,{category:$(".container .category .selected").attr("value"),label:$(".del_item").attr("data-id")},function(resp){
				    			$(".opacity").remove();
								$(".delConfirm").remove();
								if(resp.code == 1){
									$(".del_item").remove();
									setTimeout(alert(resp.msg),1000);
								}else{
									setTimeout(alert(resp.msg),1000);
								}
				    		});
				    	});
				    	$(".container  #no").click(function(){
				    		$(".opacity").remove();
							$(".delConfirm").remove();
				    	});
					});
				});
			}
	}
})(jQuery);

/**
 * 增加某分类下的标签
 */
(function($) {
	$.koorz.addLabels = {
			conf:{
				input:".container .categoryContent  .labels .list .input",
				url:"/admin/Category/addLabel"
			},
			init:function(){
				var self = $.koorz.addLabels;
				var input = $(self.conf.input);
				input.keyup(function(e) {
					e = e || window.event;
			   		var key = e ? (e.charCode || e.keyCode) : 0;
			   		if(key == 13) {
			   			self.addLabels($(this).val());
			   		}
			   		else if((key>=48 && key<=57) 
							|| (key>=96 &&key<=105) 
							|| (key>=65 && key<=90)){
					}
		        });
				$("#addlabelButton").click(function(){
					self.addLabels(input.val());
				});
			},
			addLabels:function(val){
				var self = $.koorz.addLabels;
				if(val == ""){
					alert("标签不可为空");
				}else{
					$.get(self.conf.url,{category:$(".container .category .selected").attr("value"),label:val},function(resp){
						if(resp.code == 1){
							$(".container .categoryContent  .labels .list .input").attr("value","");
							var html = "";
							html += '<div class="item" data-id='+resp.id+'>'+val+"&nbsp;";
							html+='<a href="javascript:void(0)"   data-val='+val+'>X';
							html += '</a></div>';
							$(".items").append(html);
							setTimeout(alert(resp.msg),1000);
						}else{
							$(".container .categoryContent  .labels .list .input").attr("value","");
							setTimeout(alert(resp.msg),1000);
						}
						
					});
				}
			}
	}
})(jQuery);
/**
 * 标签上下页
 */
(function($) {
	$.koorz.labelsPrePage = {
			conf:{
				preid:".container .categoryContent  .labels .page-operation #prePage",
				nextid:".container .categoryContent  .labels .page-operation #nextPage",
				url:"/admin/Category/CategotyLabel"
			},
			init:function(){
				var self = $.koorz.labelsPrePage;
				$(self.conf.preid).click(function(){
					if($(self.conf.preid).attr("value") == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#labels";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".container .category .selected").attr("value"),page:--num};
						loadpage.load();
					}
				});
				$(self.conf.nextid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					var nextpage = $(self.conf.nextid).attr("value");
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#labels";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".container .category .selected").attr("value"),page:++num};
						loadpage.load();
					}
				});
				
			}
	}
})(jQuery);
/**
 * 右侧推荐标签
 */
(function($) {
	$.koorz.rightLabels = {
			conf:{
				preid:".container .biglabels .LabelsFalls #prePage",
				nextid:".container .biglabels .LabelsFalls #nextPage",
				url:"/admin/Category/AllLabels"
			},
			init:function(){
				var self = $.koorz.rightLabels;
				var loadpage = $.koorz.loadpage;
				loadpage.conf.id="#LabelsFalls";
				loadpage.conf.url = self.conf.url;
				loadpage.conf.ajaxData = {page:1};
				loadpage.load();
			},
			add:function(){
				$(".LabelsFalls .labelslist").find(".item").each(function(i,ele){
					var thiz = $(this);
					thiz.click(function(){
						$.koorz.addLabels.addLabels(thiz.attr("value"));
					});
				});
			},
			turnPage:function(){
				var self = $.koorz.rightLabels;
				$(self.conf.preid).click(function(){
					if($(self.conf.preid).attr("value") == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#LabelsFalls";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {page:--num};
						loadpage.load();
					}
				});
				$(self.conf.nextid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					var nextpage = $(self.conf.nextid).attr("value");
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#LabelsFalls";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {page:++num};
						loadpage.load();
					}
				});
			}
	}
})(jQuery);

/**************************************分类标签 分界线 数据抓取*****************************************/

/**
 * 抓取网站任务页面
 */
(function($) {
	$.koorz.crwalIndex = {
			conf:{
				
			},
			init:function(){
				var self = $.koorz.crwalIndex;
				$(".container .crawlContent .source").find("li").each(function(i,ele){
					$(this).click(function(){
						var thiz = $(this);
						$(".selected").removeClass("selected");
						thiz.addClass("selected");
						$(".task .setinterval #interval").attr("value",thiz.attr("interval"));
						$(".task .setTime #time").attr("value",thiz.attr("start-time"));
						if(thiz.attr("status") == "0"){
							$(".crawlContent .task .start").show();
							$(".crawlContent .task .stop").hide();
						}else{
							$(".crawlContent .task .start").hide();
							$(".crawlContent .task .stop").show();
						}
					});
				});
			},
			task:function(){
				$(".crawlContent .task #start").click(function(){
					if($(".task .setinterval #interval").attr("value") == ""){
						alert("请设置时间间隔");
					}else{
						$(".source .selected").attr("status",1);
						$(".crawlContent .task .start").hide();
						$(".crawlContent .task .stop").show();
						
						$.get("/admin/crawl/crawl",{type:$(".source .selected").attr("value"),interval:$(".task .setinterval #interval").attr("value")});
					    
					}
				});
			},
			stop:function(){
				$(".crawlContent .task #stop").click(function(){
					$.get("/admin/crawl/stop",{type:$(".source .selected").attr("value")},
							function(resp){
						$(".source .selected").attr("status",0);
						$(".crawlContent .task .start").show();
						$(".crawlContent .task .stop").hide();
						alert(resp.msg);
					});
				});
			}
	}
})(jQuery);

/**************************************数据抓取 分界线 相似单品*****************************************/

/**
 * 相似单品搭配首页
 */
(function($) {
	$.koorz.similar = {
			conf:{
				preid:".container .similarContent .style #prePage",
				nextid:".container .similarContent .style #nextPage",
				showUrl:"/admin/SimilarItems/show",
				detailUrl:"/admin/SimilarItems/detail"
			},
			init:function(){
				var self = $.koorz.similar;
				$(".container .similarContent .type").find("li").each(function(i,ele){
					$(this).click(function(){
						var thiz = $(this);
						$(".selected").removeClass("selected");
						thiz.addClass("selected");
						self.show(thiz.attr("value"));
					});
				});
			},
			show:function(val){
				var self = $.koorz.similar;
				var loadpage = $.koorz.loadpage;
				loadpage.conf.id = "#style";
				loadpage.conf.url = self.conf.showUrl;
				loadpage.conf.ajaxData = {type:val,page:1};
				loadpage.load();
			},
			turnPage:function(){
				var self = $.koorz.similar;
				$(self.conf.preid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					if(prepage == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#style";
						loadpage.conf.url=self.conf.showUrl;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {type:$(".selected").attr("value"),page:--num};
						loadpage.load();
					}
				});
				$(self.conf.nextid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					var nextpage = $(self.conf.nextid).attr("value");
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#style";
						loadpage.conf.url=self.conf.showUrl;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {type:$(".selected").attr("value"),page:++num};
						loadpage.load();
					}
				});
			},
			detail:function(){
				var self = $.koorz.similar;
				$(".container .similarContent .style .items").find("dt").each(function(i,ele){
					$(this).click(function(){
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#style";
						loadpage.conf.url=self.conf.detailUrl;
						loadpage.conf.ajaxData = {id:$(this).attr("id"),page:1};
						loadpage.load();
					});
					
				});
			},
			search:function(){
				$(".container .similarContent .search #gosearch").click(function(){
					if($("#input").attr("value") == ""){
						alert("搜索条件不可为空");
					}else{
						var loadpage = $.koorz.loadpage;
						loadpage.conf.id="#result";
						loadpage.conf.url="/admin/SimilarItems/search";
						loadpage.conf.ajaxData = {keyword:$("#input").attr("value")};
						loadpage.load();
					}
				});
			}
	}
})(jQuery);

/**************************************相似单品 分界线 用户管理*****************************************/

/**
 * 用户管理页面
 */
(function($) {
	$.koorz.user = {
			conf:{
				preid:".userPart .userFalls #prePage",
				nextid:".userPart .userFalls #nextPage",
				url:"/admin/User/showUser",
			},
			init:function(){
				var self = $.koorz.user;
				$(".container .UserContent .action").find("li").each(function(i,ele){
					$(this).click(function(){
						var thiz = $(this);
						$(".selected").removeClass("selected");
						thiz.addClass("selected");
						var loadpage = $.koorz.loadpage;
						loadpage.conf.id="#userPart";
						loadpage.conf.url=self.conf.url;
						loadpage.conf.ajaxData={id:thiz.attr("id"),page:1};
						loadpage.load();
					});
				});
			},
			createUser:function(){
				var self = $.koorz.user;
				$(".container .UserContent .action #createUser").click(function(){
					var html = '<div class="opacity">';
					html += '<div class="newUser">';
					html += '<form id="createUserForm" action="/admin/User/createUser" method="post" enctype="multipart/form-data">';
					html += '<div id="close" style="margin-left: 280px;cursor: pointer;font-size: 16px;">X</div>';
					html += '<div style="margin-left: 20px;"><input type="radio" checked="checked" name="type" value="0" />普通用户';
					html += '<input type="radio" name="type" value="1" />编辑';
					html += '<input type="radio" name="type" value="2" />管理员</div>';
					html += '<div class="patton"><span id="name">名字：</span><input type="text" id="nameInput" name="name"></div>';
					html += '<div class="patton"><span id="nickname">昵称：</span><input type="text" id="nicknameInput" name="nickname"></div>';
					html += '<div class="patton"><span id="gender">性别：</span><input type="text" id="genderInput" name="gender"></div>';
					html += '<div style="margin-left: 16px;"><span id="signature">*签名：</span><input type="text" id="signatureInput" name="signature"></div>';
					html += '<div style="margin-left: 16px;"><span id="blog">*博客：</span><input type="text" id="blogInput" name="blog"></div>';
					html += '<div style="margin-left: 16px;"><span id="weibo">*微博：</span><input type="text" id="weiboInput" name="weibo"></div>';
					html += '<div style="margin-left: 16px;"><span id="password">*密码：</span><input type="text" id="passwordInput" name="password"></div>';
					html += '<div class="patton"><span id="avatar">头像：</span><input type="file" id="avatarInput" name="avatar"></div>';
					html += '<div style="margin-top: 20px;"><span id="confirm" style="margin-left: 100px;cursor: pointer;">确定</span><span id="cancel" style="margin-left: 20px;cursor: pointer;">取消</span></div>';
					html += '</form>';
					html += '</div></div>';
					$("#container").append(html);
					$(".newUser #close").click(function(){
						$(".opacity").remove();
						$(".newUser").remove();
					});
					$("#avatarInput").change(function(){
						if($.koorz.file.pic_validate($("input[name='avatar']").val()))
						{
							return false;
						}
					else
						{
							$(this).val("");
							alert("文件格式不对！");
						}
					});
					$(".newUser #confirm").click(function(){
						if(self.checkData($("#nameInput").val()) &&  self.checkData($("#nicknameInput").val()) && self.checkData($("#genderInput").val())){
							var options = {
									success:self.onComplete,
									resetForm: true,
									clearForm:false
							};
							$(".newUser #createUserForm").ajaxSubmit(options);
						}else{
							alert("请填写完必须填写的信息");
						}
					});
					$(".newUser #cancel").click(function(){
						$(".opacity").remove();
						$(".newUser").remove();
					});
				});
			},
			checkData:function(val){
				if(val == ""){
					return 0;
				}else{
					return 1;
				}
			},
			onComplete:function(resp, status){
				$(".opacity").remove();
				$(".newUser").remove();
				alert(resp.msg);
			},
			turnPage:function(){
				var self = $.koorz.user;
				$(self.conf.preid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					if(prepage == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#userPart";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".selected").attr("id"),page:--num};
						loadpage.load();
					}
				});
				$(self.conf.nextid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					var nextpage = $(self.conf.nextid).attr("value");
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#userPart";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".selected").attr("id"),page:++num};
						loadpage.load();
					}
				});
			},
			searchUser:function(){
				var self = $.koorz.user;
				$(".container .UserContent .action #searchUser").click(function(){
					var html = '<div class="opacity">';
					html += '<div class="findUser" id="findUser">';
					html += '<div id="close" style="margin-left: 280px;font-size: 16px;cursor: pointer;">X</div>';
					html += '<div style="margin-left: 30px;"><span>关键词：</span><input type="text" id="key"></div>';
					html += '<div style="margin-left: 80px;"><input type="radio" checked="checked" name="key" value="ID" />ID';
					html += '<input type="radio" name="key" value="name" />名字';
					html += '<input type="radio" name="key" value="nickname" />昵称</div>';
					html += '<div style="margin-left: 100px;margin-top: 30px;"><button id="startSearch">开始搜索</button></div>';
					html += '</div></div>';
					$("#container").append(html);
					$(".findUser #close").click(function(){
						$(".opacity").remove();
						$(".findUser").remove();
					});
					$(".findUser #startSearch").click(function(){
						if($(".findUser #key").val() == ""){
							alert("关键词不可为空！")
						}else{
							var str=document.getElementsByName("key");
							var way = "";
							for(var i = 0;i<str.length;i++){
								if(str[i].checked == true) 
								{ 
								  way = str[i].value;
								} 
							}
							$.get("/admin/User/HasUser",{keyword:$(".findUser #key").val(),way:way},function(resp){
								if(resp.code == 1){
									var loadpage = $.koorz.loadpage;
									loadpage.conf.id="#findUser";
									loadpage.conf.url="/admin/User/searchUser";
									loadpage.conf.ajaxData = {keyword:$(".findUser #key").val(),way:way};
									loadpage.load();
								}else{
									alert(resp.msg);
								}
							});
						}
					});
				});
			},
			updateUser:function(){
				var self = $.koorz.user;
				$(".searchUserInfo #close").click(function(){
					$(".opacity").remove();
					$(".findUser").remove();
				});
				$(".searchUserInfo #updateUserInfo").click(function(){
					$(".opacity").remove();
					$(".findUser").remove();
					var thiz = $(this);
					 var html = '<div class="opacity">';
						html += '<div class="newUser" style="height: 400px;">';
						html += '<form id="updateUserForm" action="/admin/User/updateUser" method="post" enctype="multipart/form-data">';
						html += '<div id="close" style="margin-left: 280px;cursor: pointer;font-size: 16px;">X</div>';
						html += '<div><img src='+thiz.attr("avatar")+'></div>';
						html += '<input style="display:none;" type="text" id="avatar" name="avatar" value="'+thiz.attr("avatar")+'">';
						html += '<input style="display:none;" type="text" id="avatar_large" name="avatar_large" value="'+thiz.attr("avatar_large")+'">';
						html += '<div style="margin-left: 34px;"><span id="id">id：</span><input type="text" id="idInput" name="id" value="'+thiz.attr("userid")+'" readonly></div>';
						html += '<div class="patton"><span id="name">名字：</span><input type="text" id="nameInput" name="name" value="'+thiz.attr("username")+'"></div>';
						html += '<div class="patton"><span id="nickname">昵称：</span><input type="text" id="nicknameInput" name="nickname" value="'+thiz.attr("nickname")+'"></div>';
						html += '<div class="patton"><span id="gender">性别：</span><input type="text" id="genderInput" name="gender" value="'+thiz.attr("gender")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="signature">*签名：</span><input type="text" id="signatureInput" name="signature" value="'+thiz.attr("signature")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="blog">*博客：</span><input type="text" id="blogInput" name="blog" value="'+thiz.attr("blog")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="weibo">*微博：</span><input type="text" id="weiboInput" name="weibo" value="'+thiz.attr("weibo")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="password">*密码：</span><input type="text" id="passwordInput" name="password" value="'+thiz.attr("password")+'"></div>';
						html += '<div class="patton"><span id="score">分数：</span><input type="text" id="scoreInput" name="score" value="'+thiz.attr("score")+'"></div>';
						html += '<div style="margin-left: 34px;"><span id="ip">ip：</span><input type="text" id="ipInput" name="ip" value="'+thiz.attr("ip")+'"></div>';
						html += '<div class="patton"><span id="avatar">换头像：</span><input type="file" id="avatarInput" name="img"></div>';
						html += '<div style="margin-top: 20px;"><span id="confirm" style="margin-left: 100px;cursor: pointer;">确定</span><span id="cancel" style="margin-left: 20px;cursor: pointer;">取消</span></div>';
						html += '</form>';
						html += '</div></div>';
						$("#container").append(html);
						$(".newUser #close").click(function(){
							$(".opacity").remove();
							$(".newUser").remove();
						});
						$("#avatarInput").change(function(){
							if($.koorz.file.pic_validate($("input[name='img']").val()))
							{
								return false;
							}
						else
							{
								$(this).val("");
								alert("文件格式不对！");
							}
						});
						$(".newUser #confirm").click(function(){
							if(self.checkData($("#nameInput").val()) &&  self.checkData($("#nicknameInput").val()) && self.checkData($("#genderInput").val())){
								var options = {
										success:self.onComplete,
										resetForm: true,
										clearForm:false
								};
								$(".newUser #updateUserForm").ajaxSubmit(options);
							}else{
								alert("不带*的选项不可为空");
							}
						});
						$(".newUser #cancel").click(function(){
							$(".opacity").remove();
							$(".newUser").remove();
						});
				});
			},
		 UserInfo:function(){
			 var self = $.koorz.user;
			 $(".userFalls").find("button").each(function(i,ele){
				 var thiz = $(this);
				 thiz.click(function(){
					 var html = '<div class="opacity">';
						html += '<div class="newUser" style="height: 400px;">';
						html += '<form id="updateUserForm" action="/admin/User/updateUser" method="post" enctype="multipart/form-data">';
						html += '<div id="close" style="margin-left: 280px;cursor: pointer;font-size: 16px;">X</div>';
						html += '<div><img src='+thiz.attr("avatar")+'></div>';
						html += '<input style="display:none;" type="text" id="avatar" name="avatar" value="'+thiz.attr("avatar")+'">';
						html += '<input style="display:none;" type="text" id="avatar_large" name="avatar_large" value="'+thiz.attr("avatar_large")+'">';
						html += '<div style="margin-left: 34px;"><span id="id">id：</span><input type="text" id="idInput" name="id" value="'+thiz.attr("id")+'" readonly></div>';
						html += '<div class="patton"><span id="name">名字：</span><input type="text" id="nameInput" name="name" value="'+thiz.attr("username")+'"></div>';
						html += '<div class="patton"><span id="nickname">昵称：</span><input type="text" id="nicknameInput" name="nickname" value="'+thiz.attr("nickname")+'"></div>';
						html += '<div class="patton"><span id="gender">性别：</span><input type="text" id="genderInput" name="gender" value="'+thiz.attr("gender")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="signature">*签名：</span><input type="text" id="signatureInput" name="signature" value="'+thiz.attr("signature")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="blog">*博客：</span><input type="text" id="blogInput" name="blog" value="'+thiz.attr("blog")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="weibo">*微博：</span><input type="text" id="weiboInput" name="weibo" value="'+thiz.attr("weibo")+'"></div>';
						html += '<div style="margin-left: 16px;"><span id="password">*密码：</span><input type="text" id="passwordInput" name="password" value="'+thiz.attr("password")+'"></div>';
						html += '<div class="patton"><span id="score">分数：</span><input type="text" id="scoreInput" name="score" value="'+thiz.attr("score")+'"></div>';
						html += '<div style="margin-left: 34px;"><span id="ip">ip：</span><input type="text" id="ipInput" name="ip" value="'+thiz.attr("ip")+'"></div>';
						html += '<div class="patton"><span id="avatar">换头像：</span><input type="file" id="avatarInput" name="img"></div>';
						html += '<div style="margin-top: 20px;"><span id="confirm" style="margin-left: 100px;cursor: pointer;">确定</span><span id="cancel" style="margin-left: 20px;cursor: pointer;">取消</span></div>';
						html += '</form>';
						html += '</div></div>';
						$("#container").append(html);
						$(".newUser #close").click(function(){
							$(".opacity").remove();
							$(".newUser").remove();
						});
						$("#avatarInput").change(function(){
							if($.koorz.file.pic_validate($("input[name='img']").val()))
							{
								return false;
							}
						else
							{
								$(this).val("");
								alert("文件格式不对！");
							}
						});
						$(".newUser #confirm").click(function(){
							if(self.checkData($("#nameInput").val()) &&  self.checkData($("#nicknameInput").val()) && self.checkData($("#genderInput").val())){
								var options = {
										success:self.onComplete,
										resetForm: true,
										clearForm:false
								};
								$(".newUser #updateUserForm").ajaxSubmit(options);
							}else{
								alert("不带*的选项不可为空");
							}
						});
						$(".newUser #cancel").click(function(){
							$(".opacity").remove();
							$(".newUser").remove();
						});
				 });
			 });
		 }
	}
})(jQuery);

/**************************************用户管理 分界线 权限管理*****************************************/
(function($) {
	$.koorz.role = {
			conf:{
				preid:".container .RoleContent .action #prePage",
				nextid:".container .RoleContent .action #nextPage",
				url:"/admin/Role/roleDetail",
			},
			init:function(){
				var self = $.koorz.role;
				$(".container .RoleContent .action").find("li").each(function(i,ele){
					$(this).click(function(){
						var thiz = $(this);
						$(".selected").removeClass("selected");
						thiz.addClass("selected");
						var loadpage = $.koorz.loadpage;
						loadpage.conf.id="#authPart";
						loadpage.conf.url=self.conf.url;
						loadpage.conf.ajaxData={id:thiz.attr("id"),page:1};
						loadpage.load();
					});
				});
			},
			action:function(){
				var self = $.koorz.role;
				$(".container .RoleContent .authPart .roletable td").find("button").each(function(i,ele){
					$(this).click(function(){
						
					});
					
				});
			},
			turnPage:function(){
				var self = $.koorz.role;
				$(self.conf.preid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					if(prepage == 1){
						setTimeout(alert("已经是第一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#authPart";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".selected").attr("id"),page:--num};
						loadpage.load();
					}
				});
				$(self.conf.nextid).click(function(){
					var prepage = $(self.conf.preid).attr("value");
					var nextpage = $(self.conf.nextid).attr("value");
					if( parseInt(prepage) >= parseInt(nextpage) ){
						setTimeout(alert("已经是最后一页了！"),1000);
					}else{
						var loadpage =$.koorz.loadpage;
						loadpage.conf.id="#authPart";
						loadpage.conf.url=self.conf.url;
						var num = $(self.conf.preid).attr("value");
						loadpage.conf.ajaxData = {id:$(".selected").attr("id"),page:++num};
						loadpage.load();
					}
				});
			},
	}
})(jQuery);