/**
 * 获取列表
 */
$.koorz.getDapeifall = {
	conf: {
		distance: 250,
		visitNum:0,
		page: 1,
		container:".template-item-list",
		item:".template",
		pages:"#goods-wall-pages",
		loading:"#loading-tips",
        ajaxUrl: KOORZ.path + "/style/getDapei",
        ajaxData: {
        	page:1,
        	pageSize:15,
        	type:2
        }
    },
    clear:function(type){
    	var conf = $.koorz.getDapeifall.conf;
    	conf.page=1;
    	conf.ajaxData.pageSize=15;
    	conf.ajaxData.type=type;
    	conf.visitNum=0;
    	$(self.conf.container).html("");
    	$("#right-bar").unbind("scroll", self.lazyLoad);
    },
    init: function() {
    	var self = $.koorz.getDapeifall;
		
    	//瀑布流图片初始化
    	$(self.conf.container).masonry({
            itemSelector:self.conf.item,
            isFitWidth:true
        });
    	$(self.conf.pages).hide();
    	
    	self.ajaxLoad();
		$("#right-bar").bind("scroll", self.lazyLoad);
    },
    isLoading: false,
    lazyLoad: function() {
    	var self = $.koorz.getDapeifall;
        var distanceToBottom = $(document).height() - $("right-bar").scrollTop() - $("#right-bar").height();
        if (!self.isLoading && distanceToBottom < self.conf.distance) {
        	if(self.conf.visitNum>=3){
        		$(self.conf.pages).show();
        		 $('img[data-original]').lazyload({effect:'fadeIn',failure_limit:10});
                return false;
            }
        	self.ajaxLoad();
        }
    },
    ajaxLoad: function() {
    	var self = $.koorz.getDapeifall;
		conf = $.koorz.getDapeifall.conf;
    	conf.ajaxData.page = conf.page;
        
        $.ajax({
            url: conf.ajaxUrl,
            type: "get",
            dataType: "json",
            timeout: 70000,
            data: conf.ajaxData,
            error: function(XMLHttpRequest, textStatus, errorThrown){
                $(self.conf.loading).hide();
            },
            beforeSend:function () {
            	self.isLoading = true;
            	$(self.conf.loading).show();
            },
            complete:function () {
            	self.isLoading = false;
                $(self.conf.loading).hide();
            },
            success: function(resp) {
            	if(resp.list.length == 0){
            		$("#right-bar").unbind("scroll", self.lazyLoad);
            	}else{
            		self.flowGoods(resp.list);
            		conf.page += 1;
            	}
            	conf.visitNum += 1;
            }
        });
    },
    flowGoods: function(list) {
    	var self = $.koorz.getDapeifall;
        var items = "";
        $.each(list,function(key,val) {
        	var rwidth=110;
        	var rheight=val.height*rwidth/val.width;
        	
        	items += '<div class="template" template-id="'+val.id+'">';
        	items +='<div class="template-image" style="height:'+(rheight+2)+'px;">';
        	items +='<img  wid='+val.width+' hei='+val.height+' height='+rheight+' data-original="'+val.url+'" alt="'+val.name+'">';
        	items +='</div>';
        	if(val.name!=undefined)
        		{
        			items +='<div class="template-title">'+val.name+'</div>';
        		}
        	items +="</div>"
        });
        var $boxes = $(items);
        $(self.conf.container).append( $boxes ).masonry('appended',$boxes);
        $(self.conf.container).masonry('reload');
        $('img[data-original]').each(function(i,ele){
        	$(this).click(function(){
        		$.koorz.dapei.itemclick(self.conf.ajaxData.type,$(this).attr("src"),$(this).parents(".template").attr("template-id"),$(this).attr("wid"),$(this).attr("hei"));
        	});
        	
        });
        $('img[data-original]').lazyload({effect:'fadeIn',failure_limit:10});
    }
};
/**
 * 搭配设置
*/
$.koorz.dapei={
		conf:{
			bg:".image-bg",
			ele0:"#ele0",
			ele1:"#ele1",
			ele2:"#ele3",
			ele3:"#ele4",
			ele4:"#ele5"
			
		},
		init:function(){
			$(".left-menu #step0").click(function(){
				$.koorz.getDapeifall.clear(2);
				$.koorz.getDapeifall.init();
			});
			$(".left-menu #step1").click(function(){
				$.koorz.getDapeifall.clear(0);
				$("#ele0").parent("div").find(".image-view.sel").removeClass("sel");
				$("#ele0").addClass("sel");
				$.koorz.getDapeifall.init();
			});
			$("div[id^=ele]").each(function(i,eld){
				$(this).click(function(){
					$(this).parent("div").find(".image-view.sel").removeClass("sel");
					$(this).addClass("sel");
					$.koorz.getDapeifall.clear($(this).attr("type"));
					$.koorz.getDapeifall.init();
				});
			});
			$("#submit").click(function(){
				if(($("#ele4").find("img").attr("img-id")!=undefined)&&
					($("#ele1").find("img").attr("img-id")!=undefined)&&
					($("#ele3").find("img").attr("img-id")!=undefined)&&
					($("#ele5").find("img").attr("img-id")!=undefined)&&
					($("#ele0").find("img").attr("img-id")!=undefined))
					{
						var url= KOORZ.path + "/style/insertStyle";
						var ajaxData={
							main_id:$("#image-baker .image-bg").attr("img-id"),
						    cloth:$("#ele4").find("img").attr("img-id"),
						    jean:$("#ele1").find("img").attr("img-id"),
						    shoe:$("#ele3").find("img").attr("img-id"),
						    ele:$("#ele5").find("img").attr("img-id"),
						    bag:$("#ele0").find("img").attr("img-id"),
						    desc:$("#words").val()
						};
						
						$.post(url,ajaxData,function(data){
							if(data.code==1)
								{
									$(".default-image").show();
									$("#image-baker").hide();
									$("[id^=ele]").each(function(i,ele){
										$(this).find(".close").remove();
										$(this).find("img").remove();
										$(this).find(".image-view-text").show();
										$(this).removeClass("fill");
										$("#words").val("");
										$.koorz.getDapeifall.clear(2);
										$.koorz.getDapeifall.init();
									});
								}
							else
								{
									alert(data.msg);
								}
						});
						}
					else
						{
						alert("搭配项目不全！");
						}
				
			});
		},
		itemclick:function(type,url,imgid,width,height){
			var self=$.koorz.dapei;
			if(type==2)
				{
					$(".default-image").hide();
					$("#image-baker").show();
					$("[id^=ele]").each(function(i,ele){
						$(this).find(".close").remove();
						$(this).find("img").remove();
						$(this).find(".image-view-text").show();
						$(this).removeClass("fill");
						$(this).removeClass("sel");
					});
					
					$(self.conf.bg).attr("src",url);
					$(self.conf.bg).attr("img-id",imgid);
					
				}
			else
				{
					var id="#ele"+type;
					var cwidth=$(id).attr("width");
					var cheight=$(id).attr("height");
					var rtop=0;
					var rleft=0;
					var rwidth=cwidth;
					var rheight=cheight;
					if(width/height>cwidth/cheight)
						{
							rheight=height*rwidth/width;
							rtop=(cheight-rheight)/2;
						}
					else
						{
							rwidth=width*rheight/height;
							rleft=(cwidth-rwidth)/2;
						}
					$(id).find(".image-view-text").hide();
					if($(id).find("img").attr("src"))
						{
							$(id).find("img").attr("src",url);
						}
					else
						{
							var img="";
							img+='<img img-id='+imgid+' src="'+url+'" style="position:absolute;width: '+rwidth+'px; height: '+rheight+'px;top:'+rtop+'px;left:'+rleft+'px; margin: 0px;">';
							img+='<span class="close" style="top: 0px; right: 13px;position:absolute">x</span>';
							$(id).append(img);
							$(id).addClass("fill");
							$(id).find(".close").click(function(){
								$(id).removeClass("fill");
								$(id).find("img").remove();
								$(this).remove();
								$(id).find(".image-view-text").show();
							});
							
						}
				}
		}
}