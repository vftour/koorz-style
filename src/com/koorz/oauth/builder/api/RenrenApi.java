package com.koorz.oauth.builder.api;

import com.koorz.oauth.extractors.AccessTokenExtractor;
import com.koorz.oauth.extractors.RenrenTokenExtractor;
import com.koorz.oauth.model.OAuthConfig;
import com.koorz.oauth.utils.OAuthEncoder;
import com.koorz.utils.LoginUtil;

public class RenrenApi extends DefaultApi20 {
	private static final String AUTHORIZE_URL = "https://graph.renren.com/oauth/authorize?client_id=%s&response_type=code&redirect_uri=%s&state=%s&x_renew=true";
	private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL
			+ "&scope=%s";

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new RenrenTokenExtractor();
	}

	@Override
	public String getAccessTokenEndpoint() {
		return "https://graph.renren.com/oauth/token?grant_type=authorization_code";
	}

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		// Append scope if present
		if (config.hasScope()) {
			return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					OAuthEncoder.encode(LoginUtil.createState(config.getState())),
					OAuthEncoder.encode(config.getScope()));
		} else {
			return String.format(AUTHORIZE_URL, config.getApiKey(),
					OAuthEncoder.encode(config.getCallback()),
					OAuthEncoder.encode(LoginUtil.createState(config.getState())));
		}
	}
}
