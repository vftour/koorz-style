package com.koorz.oauth.oauth;

import com.koorz.oauth.model.Token;
import com.koorz.oauth.model.Verifier;

/**
 * The main Scribe object. 
 * 
 * A facade responsible for the retrieval of request and access tokens and for the signing of HTTP requests.  
 * 
 * @author Pablo Fernandez
 * @param <T>
 */
public interface OAuthService
{

  /**
   * Retrieve the access token
 * @param <T>
 * @param <T>
   * 
   * @param requestToken request token (obtained previously)
   * @param verifier verifier code
   * @return access token
   */
  public Token<?> getAccessToken(Verifier verifier);

  /**
   * Returns the OAuth version of the service.
   * 
   * @return oauth version as string
   */
  public String getVersion();
  
  /**
   * Returns the URL where you should redirect your users to authenticate
   * your application.
   * 
   * @param requestToken the request token you need to authorize
   * @return the URL where you should redirect your users
   */
  public String getAuthorizationUrl();
}
