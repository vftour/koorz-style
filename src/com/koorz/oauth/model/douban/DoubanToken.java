/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model.douban;

import com.koorz.oauth.model.Token;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-4-8 下午4:14:05
 * 版  本  号：1.0
 */
public class DoubanToken extends Token<DoubanToken>{
	private static final long serialVersionUID = 5272152167910815684L;
	private String uid = null;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public DoubanToken() {
		super();
	}
	public DoubanToken(String accessToken, int expiresIn, String refreshToken,String uid) {
		super(accessToken, expiresIn, refreshToken);
		this.uid = uid;
	}
}
