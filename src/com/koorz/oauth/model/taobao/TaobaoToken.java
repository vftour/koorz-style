/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.oauth.model.taobao;

import com.koorz.oauth.model.Token;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-31 下午11:07:08
 * 版  本  号：1.0
 */
public class TaobaoToken extends Token<TaobaoToken>{
	private static final long serialVersionUID = 5445698402566464654L;
	private String uid = null;
	private String nick = null;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public TaobaoToken() {
		super();
	}
	
	public TaobaoToken(String accessToken, int expiresIn, String refreshToken,String userId,String nickname) {
		super(accessToken, expiresIn, refreshToken);
		this.uid = userId;
		this.nick = nickname;
	}
}
