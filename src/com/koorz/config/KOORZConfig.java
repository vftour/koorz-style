package com.koorz.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.koorz.handler.RequestContextHandler;
import com.koorz.interceptor.resource.ResourcePlugin;
import com.koorz.jfinal.route.AutoBindRoutes;
import com.koorz.jfinal.tablebind.AutoTableBindPlugin;
import com.koorz.jfinal.tablebind.SimpleNameStyles;
import com.koorz.utils.SystemConfig;
import com.koorz.web.BaseController;
/**
 * API引导式配置
 */
public class KOORZConfig extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		me.setDevMode(SystemConfig.getToBool("devMode", false));
		me.setError404View("/40X.html");
		me.setError500View("/50X.html");
		me.setBaseViewPath("/WEB-INF/view");
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		AutoBindRoutes abr = new AutoBindRoutes();
		abr.addExcludeClass(BaseController.class);
		me.add(abr);
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(SystemConfig.getValue("jdbc.url"), SystemConfig.getValue("jdbc.username"), SystemConfig.getValue("jdbc.password"));
		druidPlugin.addFilter(new StatFilter());
		WallFilter wall = new WallFilter();
		wall.setDbType(SystemConfig.getValue("jdbc.dbType"));
		druidPlugin.addFilter(wall);
		me.add(druidPlugin);
		
//		DruidPlugin druidPlugin2 = new DruidPlugin(SystemConfig.getValue("jdbc.url2"), SystemConfig.getValue("jdbc.username2"), SystemConfig.getValue("jdbc.password2"));
//		druidPlugin2.addFilter(new StatFilter());
//		WallFilter wall2 = new WallFilter();
//		wall2.setDbType(SystemConfig.getValue("jdbc.dbType"));
//		druidPlugin2.addFilter(wall2);
//		me.add(druidPlugin2);
//		DataSourceUtil.getInstance().addPlugin("koorz-web", druidPlugin2);
		//配置自动绑定表插件
		AutoTableBindPlugin autoTableBindPlugin = new AutoTableBindPlugin(
				druidPlugin, SimpleNameStyles.LOWER_UNDERLINE);
		//过滤不扫描的model
		//autoTableBindPlugin.setExcludeClasses(excludeClasses)
		//只扫描注解的model被自动注册
		//autoTableBindPlugin.setAutoScan(false);
		autoTableBindPlugin.setShowSql(true);
		SqlReporter.setLogger(true);
		
		me.add(autoTableBindPlugin)
		.add(new EhCachePlugin());
		me.add(new ResourcePlugin("static_resource_cfg.xml"));
		//me.add(new QuartzPlugin("resource/quartzjob.properties"));
		//me.add(new LucenePlugin());
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		//me.add(new AuthInterceptor());
		//me.add(new TxByActionKeys("/style/submit"));
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		//全局上下文
		me.add(new RequestContextHandler());
		
		//静态资源上下文路径
		//me.add(new ResourceHandler());
		/**
		 * 制定上下文路径,默认为 CONTEXT_PATH
		 */
		me.add(new ContextPathHandler());
		//访问路径是/admin/monitor
		DruidStatViewHandler dvh =  new DruidStatViewHandler("/admin/monitor", new IDruidStatViewAuth() {
			public boolean isPermitted(HttpServletRequest request) {//获得查看权限
//				HttpSession hs = request.getSession(false);
//				return (hs != null && hs.getAttribute("$admin$") != null);
//				return true;
				HttpSession hs = request.getSession(false);
				return (hs != null && hs.getAttribute("USER_CONTEXT") != null);
			}
		});
		me.add(dvh);
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebContent", 8081, "/", 5);
	}
}
