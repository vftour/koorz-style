/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import com.jfinal.plugin.IPlugin;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午11:17:35
 * 版  本  号：1.0
 */
public class LucenePlugin implements IPlugin{

	@Override
	public boolean start() {
		try {
			IndexRebuilder.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean stop() {
		try {
			IndexRebuilder.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

}
