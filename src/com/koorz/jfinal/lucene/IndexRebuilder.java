/**
 * 
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.jfinal.lucene;

import java.util.List;

import com.jfinal.log.Logger;
import com.koorz.utils.SystemConfig;

/**
 * 功能描述：重建索引
 * 作        者：尹东东 
 * 创建时间：2013-3-18 下午9:28:25
 * 版  本  号：1.0
 */
public class IndexRebuilder {

	private final static Logger log = Logger.getLogger(IndexRebuilder.class);
	private final static int DEFAULT_BATCH_COUNT = 1000;
    
	/**
	 * 
	 * 功能描述：初始化重建索引
	 * 作        者：尹东东
	 * 创建时间：2013-3-19 上午10:14:42
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
    public static void init() throws Exception{
    	//每批次处理对象数，提升这个数值可提醒构建的性能，但要注意内存是否足够
    	int batch_count = DEFAULT_BATCH_COUNT;
		//初始化索引管理器
		IndexHolder holder = IndexHolder.bulider().init(SystemConfig.getIndexDir());
//		Song song = new Song();
//		build(holder, song, batch_count);
//		User user = new User();
//		build(holder, user, batch_count);
		
//		holder.optimize(song.getClass());
    } 

	/**
	 * 构建索引
	 * @param holder
	 * @param objClass
	 * @param batch_count
	 * @return
	 */
	private static int build(IndexHolder holder, Searchable obj, int batch_count) throws Exception {
		int ic = 0;
		int page = 1;
		do {
			List<? extends Searchable> objs = obj.ListAfter(page, batch_count);
			if(objs != null && objs.size()>0){
				ic  += holder.add(objs);
				//last_id = objs.get(objs.size()-1).id();
				page++;
				log.info(ic + " documents of " + obj.getClass().getSimpleName() + " added.");
			}
			if(objs == null || objs.size() < batch_count)
				break;
		}while(true);
		
		return ic;
	}
	
	public static void delete() throws Exception{
		//IndexHolder.bulider().delete(Song.class);
		//IndexHolder.bulider().delete(User.class);
    }
}
