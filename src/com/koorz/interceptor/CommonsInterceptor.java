/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor;

import com.jfinal.aop.InterceptorStack;
import com.koorz.interceptor.resource.ResourceInterceptor;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-24 下午3:20:52
 * 版  本  号：1.0
 */
public class CommonsInterceptor extends InterceptorStack {

	@Override
	public void config() {
		addInterceptors(new ResourceInterceptor(),new MenuInterceptor());
	}

}
