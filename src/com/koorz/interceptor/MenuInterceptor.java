/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor;


import java.util.List;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.koorz.modules.menu.Menu;

/**
 * 功能描述：前台显示的导航菜单,只在涉及到页面的地方添加拦截
 * 作        者：尹东东 
 * 创建时间：2013-1-24 下午12:27:36
 * 版  本  号：1.0
 */
public class MenuInterceptor implements Interceptor {
	
	@Override
	public void intercept(ActionInvocation ai) {
		ai.invoke();
		List<Menu> menus = Menu.dao.getMenus();
		if(menus != null && menus.size() > 0)
		ai.getController().setAttr("menus", menus);
	}
}
