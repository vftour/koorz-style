/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor;


import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.koorz.modules.user.User;
import com.koorz.utils.SessionContext;

/**
 * 功能描述：安全拦截器
 * 作        者：尹东东 
 * 创建时间：2013-1-24 下午12:27:36
 * 版  本  号：1.0
 */
public class AuthInterceptor implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		User loginUser = SessionContext.get().user();
		if(loginUser != null){
			ai.invoke();
		}else {
			controller.renderError(404);
		}
	}

}
