/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.checkStyle;

import java.util.ArrayList;


/**
 * 功能描述：用于计算任务开始时间
 * 作        者：尹东东 
 * 创建时间：2013-5-24 下午6:13:23
 * 版  本  号：1.0
 */
public class CheckTask extends ITask{
	
	private volatile Thread linker; 
	
	private boolean isRunning = true;
	
	public CheckTask(String id){
		setId(id);
		setData(new ArrayList<String>());
	}
	
	@Override
	public void run() {
		do{
			setEndTime(System.currentTimeMillis());
			if(isTimeout()){
				setData(null);
			}
			if(!isRunning){
				break;
			}
		}while(true);
	}

	@Override
	public void start() {
		setStartTime(System.currentTimeMillis());
		linker = new Thread(this);
		linker.start();
	}

	@Override
	public void stop() {
		isRunning = false;
		setData(null);
	}
}
