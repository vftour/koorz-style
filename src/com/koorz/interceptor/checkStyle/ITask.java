/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.interceptor.checkStyle;

import java.util.List;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-24 下午6:23:27
 * 版  本  号：1.0
 */
public abstract class ITask implements Runnable{
	/** 
     * 默认一个任务的超时时间，单位为毫秒 
     */  
    private static final long DEFAULT_TASK_TIMEOUT = 1000*60*30;
    /**
     * 任务id
     */
    private String id;
    /**
     * 存放的数据
     */
    private List<String> data;
    /**
	 * 任务开始时间
	 */
	private long startTime;
	/**
	 * 任务结束时间
	 */
	private long endTime;
    
	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public String getId() {
		return id;
	}

	protected void setId(String id) {
		this.id = id;
	}

	public long getStartTime() {
		return startTime;
	}

	protected void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	protected void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	/**
	 * 
	 * 功能描述：启动任务
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午6:27:47
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
    public abstract void start();
	
	/**
	 * 
	 * 功能描述：停止任务
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午6:29:20
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public abstract void stop();
	
	/**
	 * 
	 * 功能描述：判断任务是否超时
	 * 作        者：尹东东
	 * 创建时间：2013-5-24 下午6:30:01
	 * 返       回:返回true表示已经超时
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public boolean isTimeout(){
		if((endTime - startTime) <= DEFAULT_TASK_TIMEOUT)return false;
		return true;
	}
}
