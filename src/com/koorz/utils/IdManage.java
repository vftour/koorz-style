/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 功能描述：id生成器
 * 作        者：尹东东 
 * 创建时间：2013-1-3 下午11:54:05
 * 版  本  号：1.0
 */
public class IdManage {
	public static final int ROLE = 2;
	public static final int PERMISSION = 3;
	public static final int PARTYACCOUNT = 4;
	public static final int MENU = 5;
	public static final int ROLE_PERMISSION = 6;
	public static final int IMAGE = 59;
	public static final int SILIMAR_ITEMS = 60;
	public static final int CATEGORY_IMAGE = 61;
	public static final int CATEGORY = 62;
	public static final int STYLE = 63;
	public static final int STYLE_ITEMS = 64;
	public static final int LABEL_IMAGE = 65;
	public static final int LABEL = 66;
	public static final int CATEGORY_LABEL =67;
	public static final int USER = 68;
	public static final int LOGIN_HISTORY = 70;
	public static final int LABEL_GROUP = 71;
	public static final int LABEL_GROUP_LABEL = 73;
	public static final int SIMILAR_STYLES = 74;
	public static final int CRAWL_HISTORY = 75;
	public static final int LABEL_GROUP_IMAGE = 76;
	public static final int CRAWL_WEBSITE = 77;
	
	//佣金分成
	public static final int ORDER_FORM = 80;
	public static final int COMMISSION_RATE = 81;
	public static final int DRAW_RECORD = 82;
	public static final int REWARD_RECORD = 83;
	public static final int REWARD_STANDARDS = 84;
	public static final int VIRTUAL_CURRENCY = 85;
	public static final int VIRTUAL_ACCOUNT = 86;
	
	public static synchronized String nextId(int type){
		Record model = Db.findFirst("select val from id_manage where type = ? ", new Object[]{type});
		int val = 1;
		if(model == null){
			String id = StringUtils.join(UUID.randomUUID().toString().split("-"));
			String sql = "insert into id_manage(id,type,val) values(?,?,?)";
			Db.update(sql, new Object[]{id,type,val});
		}else {
			val = Integer.parseInt(model.getStr("val"))+1;
		}
		String sql = "update id_manage set val=? where type=? ";
		Db.update(sql, new Object[]{val,type});
		return (100000 + val -1)+"";
	}
}
