/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.IPlugin;


/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-6-6 下午11:08:47
 * 版  本  号：1.0
 */
public class DataSourceUtil {
	//key为数据源名称
	private Map<String,IPlugin> plugins = new HashMap<String,IPlugin>();
	private static class SingletonHolder { 
		static final DataSourceUtil INSTANCE = new DataSourceUtil(); 
	}
	
	public static DataSourceUtil getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	private DataSourceUtil() {}

	public IPlugin getPlugin(String key) {
		if (!plugins.isEmpty()) {
			return plugins.get(key);
		}
		return null;
	}

	public void addPlugin(String key,IPlugin plugin) {
		plugins.put(key, plugin);
	}
}
