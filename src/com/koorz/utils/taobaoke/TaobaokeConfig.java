package com.koorz.utils.taobaoke;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;


public class TaobaokeConfig {
	
	public static String appkey = "1973292355";
	public static String secret = "3baedfa438134ca62fb0f022b7fb4cef";
	public static String timestamp = String.valueOf(System.currentTimeMillis());
	private static String cacheSign = "";
	/**
	 * $secret.'app_key'.$app_key.'timestamp'.$timestamp.$secret;
	 * @return
	 */
	static {
		StringBuilder signStr = new StringBuilder();
		signStr.append(secret)
		.append("app_key").append(appkey)
		.append("timestamp").append(timestamp)
		.append(secret);
        try {
        	cacheSign = byte2hex(HMacMD5.getHmacMd5Bytes(signStr.toString().getBytes("utf-8"),secret.getBytes("utf-8")));
        } catch (Exception ex) {
            throw new java.lang.RuntimeException("sign error !");
        }
	}
	public static String getSign(){
        return cacheSign;
	}
	public static String getOriginalSign(){
		String result = null;
		StringBuilder signStr = new StringBuilder();
		signStr.append(secret)
		.append("app_key").append(appkey)
		.append("timestamp").append(timestamp)
		.append(secret);
        try {
        	result = signStr.toString();
        } catch (Exception ex) {
            throw new java.lang.RuntimeException("sign error !");
        }
        return result;
	}
	private static String byte2hex(byte[] data){
		StringBuffer hs = new StringBuffer();
        String stmp = "";
        for (int n = 0; n < data.length; n++) {
            stmp = (java.lang.Integer.toHexString(data[n] & 0XFF));
            if (stmp.length() == 1)
                hs.append("0").append(stmp);
            else
                hs.append(stmp);
        }
        return hs.toString().toUpperCase();
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		System.out.println(byte2hex(HMacMD5.getHmacMd5Bytes("603084a3335aa43cdafb169d36279d60app_key12364270timestamp1338516358959603084a3335aa43cdafb169d36279d60".getBytes("utf-8"),secret.getBytes("utf-8"))));
	}
}
