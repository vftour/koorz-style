package com.koorz.utils.net;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet4Address;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.commons.io.FileUtils;


/**
 * 
 * 功能描述：QQWry.DAT 加载时间大概在50ms左右 
一次查询时间4ms左右 
速度98w/s不缓存 
另外IPLocation是个不可变对象以及创建就不可更改 
基本内存开销至少在10M左右 这个空间换效率的做法 很值 因为 一般的VPS的内存都不会低于1G 
 * 作        者：尹东东 
 * 创建时间：2013-4-16 下午10:55:30
 * 版  本  号：1.0
 */
public class IPSeeker {
	final ByteBuffer buffer;
	final Helper h;
	final int offsetBegin, offsetEnd;

	public IPSeeker(String path) throws IOException {
		File f = new File(path);
		if (f.exists()) {
			buffer = ByteBuffer.wrap(FileUtils.readFileToByteArray(f));
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			offsetBegin = buffer.getInt(0);
			offsetEnd   = buffer.getInt(4);
			if (offsetBegin == -1 || offsetEnd == -1) {
				throw new IllegalArgumentException("File Format Error");
			}
			h = new Helper(this);
		} else {
			throw new FileNotFoundException();
		}
	}
	
	public IPLocation getLocation(final byte ip1, final byte ip2, final byte ip3, final byte ip4) {
		return getLocation(new byte[] { ip1, ip2, ip3, ip4 });
	}
	
	protected final IPLocation getLocation(final byte[] ip) {
		return h.getLocation(h.locateOffset(ip));
	}
	
	public IPLocation getLocation(final Inet4Address address) {
		return getLocation(address.getAddress());
	}
}