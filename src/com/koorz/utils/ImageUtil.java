/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.log.Logger;
import com.koorz.constant.GlobalConstant;
//import com.sun.imageio.plugins.bmp.BMPImageReader;
//import com.sun.imageio.plugins.gif.GIFImageReader;
//import com.sun.imageio.plugins.jpeg.JPEGImageReader;
//import com.sun.imageio.plugins.png.PNGImageReader;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-18 下午3:27:45
 * 版  本  号：1.0
 */
public class ImageUtil {
	protected final static Logger logger = Logger.getLogger(ImageUtil.class);
	//瀑布流宽度
	public static int WATERFALL_WIDTH = 290;
	//详情页宽度
	public static int DETAIL_WIDTH = 400;
	//单品相似宽度
	public static int ITEM_WIDTH = 240;
	//搭配宽高
	public static int STYLE_WIDTH = 100;
	public static int STYLE_HEIGHT = 100;
	//推荐相似宽高
	public static int SIMILAR_WIDTH = 160;
	public static int SIMILAR_HEIGHT = 160;
	
	//用户头像
	public static int AVATAR_LARGE_WIDTH = 100;
	public static int AVATAR_LARGE_HEIGHT = 100;
	public static int AVATAR_WIDTH = 32;
	public static int AVATAR_HEIGHT = 32;
	
	/**
	 * 
	 * 功能描述：裁剪图片
	 * 作        者：尹东东
	 * 创建时间：2013-5-18 下午3:40:23
	 * 参       数:src原文件地址,dest新生成文件地址，width要生成文件的宽度，height要生成文件的高度
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 * @throws IM4JavaException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void compress(String src,String dest,Integer width,Integer height) throws IOException, InterruptedException, IM4JavaException{
		IMOperation op = new IMOperation();
        op.addImage(src);
        op.resize(width, height);
        op.addImage(dest);
        ConvertCmd convert = new ConvertCmd(true);
        String os = SystemConfig.getValue("os");
    	if(StringUtils.equals(os, "win")){
    		convert.setSearchPath(SystemConfig.getValue("GraphicsMagick_path"));
    	}
    	convert.run(op);
	}
	
//	public static JSONObject saveAvatar(String src,String dest,Integer width,Integer height,String userId){
//		String dir = String.format(SystemConfig.getValue("user.avatar_dir"), userId);
//		if(createDir(dir)){
//			try {
//				compress(src,dest,width,height);
//				try {
//					File destFile = new File(dest);
//					Image destImg = ImageIO.read(destFile);
//					JSONObject jsonObj = new JSONObject();
//					jsonObj.put("width", destImg.getWidth(null));
//					jsonObj.put("height", destImg.getHeight(null));
//					jsonObj.put("url", GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(dest, "user"+"/"));
//					return jsonObj;
//				} catch (IOException e) {
//					e.printStackTrace();
//					logger.error("读取目标文件失败", e);
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//				logger.error("裁剪失败，可能文件不存在", e);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//				logger.error("裁剪失败，InterruptedException", e);
//			} catch (IM4JavaException e) {
//				e.printStackTrace();
//				logger.error("裁剪失败，IM4JavaException", e);
//			}
//		}
//		return null;
//	}
//	
//	public static JSONObject saveImage(String src,String dest,Integer width,Integer height){
//		try {
//			compress(src,dest,width,height);
//			try {
//				File destFile = new File(dest);
//				Image destImg = ImageIO.read(destFile);
//				JSONObject jsonObj = new JSONObject();
//				jsonObj.put("width", destImg.getWidth(null));
//				jsonObj.put("height", destImg.getHeight(null));
//				jsonObj.put("url", GlobalConstant.RESOURCES_HOST+"/images/"+destFile.getName());
//				return jsonObj;
//			} catch (IOException e) {
//				e.printStackTrace();
//				logger.error("读取目标文件失败", e);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//			logger.error("裁剪失败，可能文件不存在", e);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//			logger.error("裁剪失败，InterruptedException", e);
//		} catch (IM4JavaException e) {
//			e.printStackTrace();
//			logger.error("裁剪失败，IM4JavaException", e);
//		}
//		return null;
//	}
//	
//	public static void delFile(String filePath){
//		File file = new File(filePath);
//		file.delete();
//	}
//	
//	/**
//	 * 
//	 * 功能描述：根据url获取本地文件路径  
//	 * http://static.koorz.com/images/100779_waterfall.jpeg
//	 * http://static.koorz.com/100252/avatar/100252_small.jpeg
//	 * 作        者：尹东东
//	 * 创建时间：2013-6-6 下午12:16:59
//	 * 参       数:
//	 * U R L:
//	 * 返       回:无
//	 * 异       常：无
//	 * 版  本 号：1.0
//	 */
//	public static String getLocalPath(String url){
//		if(StringUtils.contains(url, "avatar")){
//			String userId = "temp-0";
//			//D:/resources/koorz/user/%s/avatar
//			String dest = String.format(SystemConfig.getValue("user.avatar_dir"), userId);
//			return StringUtils.substringBefore(dest, "/"+userId) + StringUtils.substringAfter(url, GlobalConstant.RESOURCES_HOST);
//		}else {
//			//img_dir=D:/resources/koorz/images
//			String dest = SystemConfig.getValue("img_dir");
//			return StringUtils.substringBefore(dest, "/images") + StringUtils.substringAfter(url, GlobalConstant.RESOURCES_HOST);
//		}
//	}
//	
//	/**
//	 * 
//	 * 功能描述：根据本地文件路径生成网络访问的路径
//	 * 作        者：尹东东
//	 * 创建时间：2013-6-6 下午1:30:38
//	 * 参       数:
//	 * 返       回:无
//	 * 异       常：无
//	 * 版  本 号：1.0
//	 */
//	public static String getNetPath(String path){
//		if(StringUtils.contains(path, "avatar")){
//			return GlobalConstant.RESOURCES_HOST+StringUtils.substringAfter(path, "user");
//		}else {
//			return GlobalConstant.RESOURCES_HOST+"/images/"+new File(path).getName();
//		}
//	}
//	
//	/**
//	 * 
//	 * 功能描述：只支持jpeg、png、bmp
//	 * 作        者：尹东东
//	 * 创建时间：2013-2-2 下午3:51:30
//	 * 参       数：
//	 * 返       回:无
//	 * 异       常：无
//	 * 版  本 号：1.0
//	 */
//	public static String getImgType(byte[] mapObj) throws IOException {
//		String type = "";
//		ByteArrayInputStream bais = null;
//		MemoryCacheImageInputStream mcis = null;
//		try {
//			bais = new ByteArrayInputStream(mapObj);
//			mcis = new MemoryCacheImageInputStream(bais);
//			Iterator<ImageReader> itr = ImageIO.getImageReaders(mcis);
//			while (itr.hasNext()) {
//				ImageReader reader = (ImageReader) itr.next();
//				if (reader instanceof GIFImageReader) {
//					type = "gif";
//				} else if (reader instanceof JPEGImageReader) {
//					type = "jpeg";
//				} else if (reader instanceof PNGImageReader) {
//					type = "png";
//				} else if (reader instanceof BMPImageReader) {
//					type = "bmp";
//				}
//			}
//		} finally {
//			if (bais != null) {
//				try {
//					bais.close();
//				} catch (IOException ioe) {
//					ioe.printStackTrace();
//				}
//			}
//			if (mcis != null) {
//				try {
//					mcis.close();
//				} catch (IOException ioe) {
//					ioe.printStackTrace();
//				}
//			}
//		}
//		return type;
//	}
	
	public static byte[] getImageBinary(String imageUrl){
		try {
			URL url = new URL(imageUrl);
			return IOUtils.toByteArray(url.openStream());
		} catch (Exception e) {
			logger.error("getImageBinary失败", e);
		}
		return null;
	}
	
	public static boolean createDir(String dir){
		boolean flag = true;
		File file = new File(dir);
		if(!file.exists()){
			flag = file.mkdirs();
		}
		return flag;
	}
}
