/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.utils;

import info.monitorenter.cpdetector.io.ASCIIDetector;
import info.monitorenter.cpdetector.io.CodepageDetectorProxy;
import info.monitorenter.cpdetector.io.JChardetFacade;
import info.monitorenter.cpdetector.io.ParsingDetector;
import info.monitorenter.cpdetector.io.UnicodeDetector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.UUID;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.koorz.constant.GlobalConstant;
import com.koorz.modules.user.User;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-1-14 下午8:39:49
 * 版  本  号：1.0
 */
public class FileUtil {
	protected final static Logger logger = Logger.getLogger(FileUtil.class);
	private static boolean createDir(String dir){
		boolean flag = true;
		File file = new File(dir);
		if(!file.exists()){
			flag = file.mkdirs();
		}
		return flag;
	}
	
	/**
	 * 
	 * 功能描述：拷贝文件到指定目录，并重命名文件名
	 * 作        者：尹东东
	 * 创建时间：2013-1-14 下午9:54:19
	 * 参       数：src要拷贝的文件,dest目录
	 * 返       回:返回新的文件路径
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private static String copyFile(File src,String dest,boolean scaling){
		if(createDir(dest)){
			try {
				String fileName = StringUtils.join(UUID.randomUUID().toString().split("-"))+"."+StringUtils.substringAfterLast(src.getName(), ".");
	    		String path = dest+ "/" + fileName;
	    		if(scaling){
	    			if(StringUtils.endsWithIgnoreCase(src.getName(), "jpg") ||
		    				StringUtils.endsWithIgnoreCase(src.getName(), "jpeg") ||
		    				StringUtils.endsWithIgnoreCase(src.getName(), "bmp") ||
		    				StringUtils.endsWithIgnoreCase(src.getName(), "png") ||
		    				StringUtils.endsWithIgnoreCase(src.getName(), "gif")){
		    			Thumbnails.of(src)
						.size(446, 214)
						.keepAspectRatio(false)
						.toFile(path);
		    		}
	    		}else {
	    			FileUtils.copyFile(src, new File(path));
	    		}
//	    		if(StringUtils.endsWithIgnoreCase(src.getName(), "jpg") ||
//	    				StringUtils.endsWithIgnoreCase(src.getName(), "jpeg") ||
//	    				StringUtils.endsWithIgnoreCase(src.getName(), "bmp") ||
//	    				StringUtils.endsWithIgnoreCase(src.getName(), "png") ||
//	    				StringUtils.endsWithIgnoreCase(src.getName(), "gif")){
//	    			Thumbnails.of(src)
//					.size(446, 214)
//					.keepAspectRatio(false)
//					.toFile(path);
//	    		}else {
//	    			FileUtils.copyFile(src, new File(path));
//	    		}
	    		return GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(path, "user"+"/");
			} catch (IOException e) {
				logger.error("copyFil生成本地文件失败e", e);
			}
		}
		return "";
	}
	
	/**
	 * 
	 * 功能描述：保存媒体文件到用户目录,自动重命名到用户目录
	 * 作        者：尹东东
	 * 创建时间：2013-1-14 下午8:54:03
	 * 参       数：src要保存的媒体文件,userId用户id
	 * 返       回:返回新的文件路径
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String saveMedia(File src,String userId){
		String path = String.format(SystemConfig.getValue("media.root_dir"), userId);
		return copyFile(src,path,false);
	}
	
	public static String saveMedia(String urlStr,String userId){
		String dest = String.format(SystemConfig.getValue("media.root_dir"), userId);
		if(createDir(dest)){
			try {
				String fileName = StringUtils.join(UUID.randomUUID().toString().split("-"))+".mp3";
	    		String path = dest+ "/" + fileName;
	    		URL url = new URL(urlStr);
	    		FileUtils.copyURLToFile(url, new File(path));
	    		
	    		return GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(path, "user"+"/");
			} catch (Exception e) {
				logger.error("报错音频文件失败", e);
			}
		}
		return "";
	}
	
	public static String saveLrc(String urlStr,String userId){
		String dest = String.format(SystemConfig.getValue("media.root_dir"), userId);
		if(createDir(dest)){
			try {
				String fileName = StringUtils.join(UUID.randomUUID().toString().split("-"))+".lrc";
	    		String path = dest+ "/" + fileName;
	    		URL url = new URL(urlStr);
	    		FileUtils.copyURLToFile(url, new File(path));
	    		
	    		return GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(path, "user"+"/");
			} catch (Exception e) {
				logger.error("报错歌词文件失败", e);
			}
		}
		return "";
	}
	
	/**
	 * 
	 * 功能描述：保存图片到用户目录,自动重命名到用户目录
	 * 作        者：尹东东
	 * 创建时间：2013-1-14 下午8:56:39
	 * 参       数：src要保存的文件,userId用户id
	 * 返       回:返回新的文件路径
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String saveImage(File src,String userId){
		String path = String.format(SystemConfig.getValue("img.root_dir"), userId);
		return copyFile(src,path,false);
	}
	
	public static String saveCert(File src,String userId){
		String path = String.format(SystemConfig.getValue("cert.root_dir"), userId);
		return copyFile(src,path,false);
	}
	/**
	 * 
	 * 功能描述：将图片进行压缩并存储
	 * 作        者：董浩
	 * 创建时间：2013-5-22 下午10:54:25
	 * 参       数：
	 * url:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String saveimg(File src,String Id){
		String dest = String.format(SystemConfig.getValue("img_dir"));
		try {
			String imgtype = "."+StringUtils.substringAfterLast(src.getName(), ".");
			String detail_img = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
			String waterfall_img = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
			String similar_img = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
			String item_img = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
			String style_img = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
			//存储文件url
			String detail_url = "http://static.koorz.com/img/"+detail_img;
			String waterfall_url = "http://static.koorz.com/img/"+waterfall_img;
			String similar_url = "http://static.koorz.com/img/"+similar_img;
			String item_url = "http://static.koorz.com/img/"+item_img;
			String style_url = "http://static.koorz.com/img/"+style_img;
			/**
			 * 生成详情页图片
			 */
			detail_img = dest+"/"+detail_img;
			Thumbnails.of(src)   
			.size(32, 32)   //规格另定
			.keepAspectRatio(false)   
			.toFile(detail_img);
			
			/**
			 * 生成瀑布墙图片
			 */
			waterfall_img = dest+"/"+waterfall_img;
			Thumbnails.of(src)   
			.size(32, 32)   
			.keepAspectRatio(false)   
			.toFile(waterfall_img);
			
			/**
			 * 生成相似图片
			 */
			similar_img = dest+"/"+similar_img;
			Thumbnails.of(src)   
			.size(32, 32)   
			.keepAspectRatio(false)   
			.toFile(similar_img);
			
			/**
			 * 生成单品图片
			 */
			item_img = dest+"/"+item_img;
			Thumbnails.of(src)   
			.size(32, 32)   
			.keepAspectRatio(false)   
			.toFile(item_img);
			
			/**
			 * 生成搭配主图片
			 */
			style_img = dest+"/"+style_img;
			Thumbnails.of(src)   
			.size(32, 32)   
			.keepAspectRatio(false)   
			.toFile(style_img);
			
			
			Db.update("update image set detail_url=?,waterfall_url=?,similar_url=?,item_url=?,style_url=? where id=?",detail_url,waterfall_url,similar_url,item_url,style_url,Id);
			//返回结果
			JSONObject json = new JSONObject();
			json.put("msg","转换保存图片成功");
			return json.toJSONString();
			
		}catch (MalformedURLException e) {
			logger.error("保存转换图片失败", e);
		} catch (IOException e) {
			logger.error("保存转换图片失败", e);
		}
		return "";
	}
	/**
	 * 
	 * 功能描述：保存用户头像
	 * 作        者：尹东东
	 * 创建时间：2013-3-21 上午9:39:46
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String saveAvatar(File src,String userId){
		String dest = String.format(SystemConfig.getValue("user.avatar_dir"), userId);
		if(createDir(dest)){
			try {
				String imgtype = "."+StringUtils.substringAfterLast(src.getName(), ".");
				String avatarLarge = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
				String avatar = StringUtils.join(UUID.randomUUID().toString().split("-"))+imgtype;
				
				/**
				 * 生成小头像
				 */
				avatar = dest+ "/" + avatar;
				Thumbnails.of(src)   
				.size(32, 32)   
				.keepAspectRatio(false)   
				.toFile(avatar);
				
				/**
				 * 生成大头像
				 */
				avatarLarge = dest+ "/" + avatarLarge;
				Thumbnails.of(src)   
				.size(100, 100)   
				.keepAspectRatio(false)   
				.toFile(avatarLarge);
				
				//删除原来的头像
				User user = User.dao.findFirst("select u.avatar,u.avatar_large from user as u where u.id=? ",userId);
				if(user != null && !StringUtils.isEmpty(user.getStr("avatar")) && !StringUtils.isEmpty(user.getStr("avatar"))){
					String tmp = user.getStr("avatar");
					delFile(dest + StringUtils.substringAfter(tmp, "avatar"));
					
					tmp = user.getStr("avatar_large");
					delFile(dest + StringUtils.substringAfter(tmp, "avatar"));
				}
				
				//返回结果
				JSONObject json = new JSONObject();
				json.put("avatar", GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(avatar, "user"+"/"));
				json.put("avatarLarge", GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(avatarLarge, "user"+"/"));
				return json.toJSONString();
			} catch (MalformedURLException e) {
				logger.error("保存用户头像失败", e);
			} catch (IOException e) {
				logger.error("保存用户头像失败", e);
			}
		}
		return "";
	}
	
	/**
	 * 
	 * 功能描述：保存用户头像
	 * 作        者：尹东东
	 * 创建时间：2013-2-2 下午3:33:34
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String saveAvatar(String urlStr,String userId){
		String dest = String.format(SystemConfig.getValue("user.avatar_dir"), userId);
		if(createDir(dest)){
			try {
				//删除原来的头像
				User user = User.dao.findFirst("select u.avatar,u.avatar_large from user as u where u.id=? ",userId);
				if(user != null && !StringUtils.isEmpty(user.getStr("avatar")) && !StringUtils.isEmpty(user.getStr("avatar"))){
					String tmp = user.getStr("avatar");
					delFile(dest + StringUtils.substringAfter(tmp, "avatar"));
					
					tmp = user.getStr("avatar_large");
					delFile(dest + StringUtils.substringAfter(tmp, "avatar"));
				}
				
				URL url = new URL(urlStr);
//				String imgtype = "."+ImageUtil.getImgType(IOUtils.toByteArray(url.openStream()));
				String imgtype = "";
				String large = dest+ "/" + userId+"_large"+ imgtype;
				String avatar = dest+ "/" + userId+"_small"+ imgtype;
				
				/**
				 * 生成小头像
				 */
				Thumbnails.of(url)   
				.size(32, 32)   
				.keepAspectRatio(false)   
				.toFile(avatar);
				
				/**
				 * 生成大头像
				 */
				Thumbnails.of(url)   
				.size(100, 100)   
				.keepAspectRatio(false)   
				.toFile(large);
				
				//返回结果
				JSONObject json = new JSONObject();
				json.put("avatar", GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(avatar, "user"+"/"));
				json.put("avatarLarge", GlobalConstant.RESOURCES_HOST+"/"+StringUtils.substringAfter(large, "user"+"/"));
				return json.toJSONString();
			} catch (MalformedURLException e) {
				logger.error("保存用户头像失败", e);
			} catch (IOException e) {
				logger.error("保存用户头像失败", e);
			}
		}
		return "";
	}
	
	public static void delFile(File file){
		if(file != null){
			file.delete();
		}
	}
	
	public static void delFile(String filePath){
		if(!StringUtils.isEmpty(filePath)){
			File file = new File(filePath);
			file.delete();
		}
	}
	
	/**
	 * 
	 * 功能描述：获取文件编码格式
	 * 作        者：尹东东
	 * 创建时间：2013-3-22 下午3:31:17
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static String getCode(File src){
		String code = "UTF-8";
		CodepageDetectorProxy detector = CodepageDetectorProxy.getInstance();
		detector.add(new ParsingDetector(false));
		detector.add(JChardetFacade.getInstance());// 用到antlr.jar、chardet.jar
		// ASCIIDetector用于ASCII编码测定
		detector.add(ASCIIDetector.getInstance());
		// UnicodeDetector用于Unicode家族编码的测定
		detector.add(UnicodeDetector.getInstance());
		try {
			Charset charset = detector.detectCodepage(src.toURI().toURL());
			code = charset.name();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return code;
	}
	public static void main(String[] args) {
//		String avatar = "C:\\Users\\Administrator\\Desktop\\0fd9f9a127.jpg";
//		try {
//			Thumbnails.of(avatar)
//			.size(446, 281)
//			.keepAspectRatio(false)
//			.toFile("C:\\Users\\Administrator\\Desktop\\d.jpg");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String url = "http://static.miri.fm/111997/images/5c368e7347844e50989e104660dfe559.jpg";
		  //D:/resources/user/111991/images/5c368e7347844e50989e104660dfe559.jpg
		//#img.root_dir=/mnt/resources/user/%s/images
		String filename = StringUtils.substringAfter(url, "images");
		String src = String.format(SystemConfig.getValue("img.root_dir"), "111991") + filename;
		System.out.println(src);
		//FileUtil.delFile(src);
	}
	public static File createFile(String dir,String fileName){
		File file = new File(dir+File.separator+fileName);
		try {
			if(!file.exists()){
				file.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	public static File writeSDFile(String fileName,InputStream stream){
		return writeSDFile(null,fileName,stream);
	} 
	
	public static File writeSDFile(String fileDir,String fileName,InputStream stream){
			File file = null;
			OutputStream output = null;
			try {
				String dirPath = null;
				if(fileDir == null || fileDir == "" || fileDir.length() == 0){
					dirPath = SystemConfig.getValue("img_dir");
				}else {
					dirPath = SystemConfig.getValue("img_dir")+"/"+fileDir;
				}
				createDir(dirPath);
				file = createFile(dirPath,fileName);
				output = new FileOutputStream(file);
				byte buf[] = new byte[128];  
	            do {  
	                int numread = stream.read(buf);  
	                if (numread <= 0) {  
	                    break;  
	                }  
	                output.write(buf, 0, numread);  
	            } while (true);  
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(output != null){
					try {
						output.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return file;
	} 
}
