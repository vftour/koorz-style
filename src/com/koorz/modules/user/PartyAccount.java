/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：第三方账号
 * 作        者：尹东东 
 * 创建时间：2013-1-6 下午2:39:40
 * 版  本  号：1.0
 */
public class PartyAccount extends Model<PartyAccount>{
	private static final long serialVersionUID = 4338555328609219474L;
	public static final PartyAccount dao = new PartyAccount();
}
