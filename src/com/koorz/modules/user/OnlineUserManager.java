/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.jfinal.log.Logger;
import com.jfinal.plugin.ehcache.CacheKit;

/**
 * 功能描述：在线用户管理器
 * 作        者：尹东东 
 * 创建时间：2013-3-14 上午10:08:11
 * 版  本  号：1.0
 */
public class OnlineUserManager {
	private static final Logger log = Logger.getLogger(OnlineUserManager.class);
	private final static String CACHE = "online-users";
	private final static int INTERVAL = 60000; //one minute
	private final static int SESSION_TIMEOUT = 30 * 60 * 1000;	//会话超时时间30分钟
	private static Timer timer;	

	/**
	 * 
	 * 功能描述：启动在线用户状态维护线程
	 * 作        者：尹东东
	 * 创建时间：2013-3-14 上午10:38:53
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public synchronized static void init() {
		if(timer==null){
			timer = new Timer("OnlineUserManager", true);
			timer.schedule(new TimerTask(){
				@Override
				public void run() {
					//扫描出数据库中所有在线的用户名
					long curTime = System.currentTimeMillis();
					List<User> users = User.listOnlineUsers();
					if(users != null){
						for(User user : users) {
							long lastActTime = getLastActiveTime(user.getStr("id"));
							if(curTime - lastActTime >= SESSION_TIMEOUT){//会话已经超时
								User.logout(user.getStr("id"));
								setLastActiveTime(user.getStr("id"), -1L);
								log.info("=> Login Session[UID:"+user.getStr("id")+"] timeout, logout it.");
							}
						}
					}
				}}, SESSION_TIMEOUT, INTERVAL);//30分钟后开始，每分钟扫描一次
			log.info("=============> OnlineUserManager started. <=============");
		}
	}
	
	/**
	 * 
	 * 功能描述：停止在线用户状态维护线程
	 * 作        者：尹东东
	 * 创建时间：2013-3-14 上午10:38:39
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public synchronized static void destroy(){
		if(timer != null){
			timer.cancel();
			timer = null;
			log.info("=============> OnlineUserManager stopped. <=============");
		}
	}
	
	/**
	 * 
	 * 功能描述：在缓存中激活用户
	 * 作        者：尹东东
	 * 创建时间：2013-3-14 上午10:37:45
	 * 参       数：userid用户id，time登录时间
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public final static void setLastActiveTime(String userid, long time) {
		if(time > 0)
			CacheKit.put(CACHE, userid, time);
		else
			CacheKit.remove(CACHE, userid);
	}
	
	/**
	 * 
	 * 功能描述：返回上一次用户活动的时间
	 * 作        者：尹东东
	 * 创建时间：2013-3-14 上午10:37:28
	 * 参       数：userid
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private final static long getLastActiveTime(String userid) {
		Long lastTime = (Long)CacheKit.get(CACHE, userid);
		return (lastTime!=null)?lastTime:-1L;
	}
}
