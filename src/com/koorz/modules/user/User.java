/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.CryptUtils;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：用户实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-1-5 下午9:32:16
 * 版  本  号：1.0
 */
public class User extends Model<User> {
	private static final long serialVersionUID = 9140571963363204976L;
	public static final String DEFAULT_ROLE = "100001";
	public static final User dao = new User();
	
	public static void saveUser(User u){
		u.set("id", IdManage.nextId(IdManage.USER)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	public static User getUser(String column,String val){
		return dao.findFirst("select * from user where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：判断当前用户是否有访问权限
	 * 作        者：尹东东
	 * 创建时间：2013-1-24 下午12:59:35
	 * 参       数：action用户正在访问的请求
	 * 返       回:true表示可以访问，false表示不可以访问
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public boolean canVisit(String action){
		Role role = get("role");
		if(role != null){
			List<Permission> permission = role.getPermissions();
			for(Permission p:permission){
				if(action.equals(p.getStr("url"))){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * 功能描述：校验是否可以登陆
	 * 作        者：尹东东
	 * 创建时间：2013-5-25 下午10:37:56
	 * 参       数:
	 * 返       回:true表示可以登陆，false表示不可以登陆
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static boolean validate(String username,String password){
		User user = dao.findFirst("select u.id from user as u " +
				" where u.username=? and u.password=?", username,CryptUtils.getMD5(password));
		return user == null?false:true;
	}
	
	/**
	 * 
	 * 功能描述：获取在线用户列表
	 * 作        者：尹东东
	 * 创建时间：2013-3-14 上午10:26:35
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<User> listOnlineUsers(){
		return dao.find("select id,username,password from user where online=1");
	}
	
	public static User adminLogin(String username,String password){
		User user = User.dao.findFirst("select * from user as u " +
				" where u.username=? and u.password=?", username,password);
		if(user != null){
			user.put("role", Role.dao.findById(user.getStr("role_id")));
		}
		return user;
	}
	
	/**
	 * 
	 * 功能描述：设置在线状态
	 * 作        者：尹东东
	 * 创建时间：2013-5-26 上午11:08:08
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void settingOnline(String uid,String ip){
		Db.update("update user set online=1 where id=?", uid);
		new LoginHistory().set("id", IdManage.nextId(IdManage.LOGIN_HISTORY))
		.set("user_id", uid)
		.set("ip", ip)
		.set("login_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：设为离线状态
	 * 作        者：尹东东
	 * 创建时间：2013-5-21 下午5:07:39
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void logout(String userId){
		Db.update("update user set online=0 where id=?", userId);
	}
	
	/**
	 * 
	 * 功能描述：更新账户余额
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 下午6:02:44
	 * 参       数:user要更新的用户，addAmount要增加的金额
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public synchronized static void updateBalance(User user,double addAmount){
		user.set("account_balance", user.getDouble("account_balance")+addAmount);
		user.update();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(getStr("id")).toHashCode(); 
	}

	@Override
	public boolean equals(Object obj) {
		boolean flag = false; 
        if (obj != null && User.class.isAssignableFrom(obj.getClass())) { 
        	User user = (User) obj; 
            flag = new EqualsBuilder().append(getStr("id"), user.getStr("id")).isEquals(); 
        } 
        return flag; 
	}
	
	/**
	 * 功能描述：用户在线状态
	 * 作        者：尹东东 
	 * 创建时间：2013-5-25 下午10:45:46
	 * 版  本  号：1.0
	 */
	public enum OnlineState {
		OFFLINE,ONLINE
	}
	
	/**
	 * 功能描述：用户性别 性别，0：女、1：男、2：未知
	 * 作        者：尹东东 
	 * 创建时间：2013-5-27 下午8:11:27
	 * 版  本  号：1.0
	 */
	public enum Gender {
		WOMAN,MAN,UNKNOWN
	}
	
	/**
	 * 功能描述：用户类型；0正常登录注册用户、1系统马甲用户、2后台管理员
	 * 作        者：尹东东 
	 * 创建时间：2013-6-12 下午9:55:33
	 * 版  本  号：1.0
	 */
	public enum UserType {
		SELF,SOCKPUPPET,ADMIN
	}
	
	/**
	 * 功能描述：账户是否冻结
	 * 作        者：尹东东 
	 * 创建时间：2013-6-17 下午3:18:23
	 * 版  本  号：1.0
	 */
	public enum IsFreeze {
		UNFREEZE,FREEZE
	}
}
