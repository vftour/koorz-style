/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：角色
 * 作        者：尹东东 
 * 创建时间：2013-1-6 下午2:36:50
 * 版  本  号：1.0
 */
public class Role extends Model<Role>{
	private static final long serialVersionUID = -9102370446276575666L;
	public static final Role dao = new Role();

	public static void saveRole(Role r){
		r.set("id", IdManage.nextId(IdManage.ROLE)).save();
	}
	
	public static Role getRole(String column,String val){
		return dao.findFirst("select * from role where "+column+"=?",val);
	}
	
	public Role getRoleById(String id){
		Role role = dao.findById(id);
		if(role != null){
			List<Permission> ps = Permission.dao.find("select p.* from permission as p " +
					" inner join role_permission as rp on rp.permission_id=p.id " +
					" where rp.role_id=?", id);
			if(ps != null && ps.size() > 0){
				role.put("permissions", ps);
			}
		}
		return role;
	}
	
	/**
	 * 
	 * 功能描述：获取当前角色拥有的权限
	 * 作        者：尹东东
	 * 创建时间：2013-5-25 下午11:03:51
	 * 参       数:角色id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public List<Permission> getPermissions(){
		return Permission.dao.find("select p.* from permission as p " +
				" inner join role_permission as rp on rp.permission_id=p.id " +
				" where rp.role_id=?", getStr("id"));
	}
}
