/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：角色权限实体
 * 作        者：尹东东 
 * 创建时间：2013-5-25 下午2:47:39
 * 版  本  号：1.0
 */
public class RolePermission extends Model<RolePermission>{
	private static final long serialVersionUID = -8368070681788834490L;
	public static final RolePermission dao = new RolePermission();
	
	public static void saveRolePermission(RolePermission r){
		r.set("id", IdManage.nextId(IdManage.ROLE_PERMISSION)).save();
	}
	
	public static RolePermission getRolePermission(String roleId,String permissionId){
		return dao.findFirst("select * from role_permission where role_id=? and permission_id=?",roleId,permissionId);
	}
	
	/**
	 * 
	 * 功能描述：添加多个权限到角色
	 * 作        者：尹东东
	 * 创建时间：2013-6-16 下午10:55:59
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void savePermissionsToRole(String roleId,String[] permissions){
		for(String id:permissions){
			RolePermission rp = getRolePermission(roleId,id);
			if(rp == null){
				rp = new RolePermission();
				rp.set("role_id", roleId);
				rp.set("permission_id", id);
				saveRolePermission(rp);
			}
		}
	}
}
