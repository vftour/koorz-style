/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.user;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：权限
 * 作        者：尹东东 
 * 创建时间：2013-1-6 下午2:36:24
 * 版  本  号：1.0
 */
public class Permission extends Model<Permission>{
	private static final long serialVersionUID = -6362665883409611412L;
	public static final Permission dao = new Permission();
	
	public static void savePermission(Permission p){
		p.set("id", IdManage.nextId(IdManage.PERMISSION)).save();
	}
	
	public static Permission getPermission(String column,String val){
		return dao.findFirst("select * from permission where "+column+"=?",val);
	}
	
	public static int[] batchUpdate(List<Permission> permissions){
		String sql = "update permission set name=?,url=?,note=? where id=?";
		return Db.batch(sql, "name,url,note,id", permissions, 20);
	}
}
