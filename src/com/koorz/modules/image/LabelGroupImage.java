/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.image;

import com.jfinal.plugin.activerecord.Model;

/**
 * 功能描述：图片所属标签组实体
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午3:41:43
 * 版  本  号：1.0
 */
public class LabelGroupImage extends Model<LabelGroupImage>{
	private static final long serialVersionUID = -8357222949640972305L;
	public static final LabelGroupImage dao = new LabelGroupImage();
}
