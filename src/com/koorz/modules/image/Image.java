/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.image;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：图片实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-5-12 下午11:09:41
 * 版  本  号：1.0
 */
public class Image extends Model<Image>{
	private static final long serialVersionUID = -6471655050798125923L;
	public static final Image dao = new Image();
	
	public static void saveImage(Image i){
		i.set("id", IdManage.nextId(IdManage.IMAGE)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	public static Image getImage(String column,String val){
		return dao.findFirst("select * from image where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：获取所有单品数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午5:24:32
	 * 参       数:
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSingles(int page,int pageSize){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" where i.check_state=? and i.type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,Image.ImageState.SHOW.ordinal(),Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取某一分类下的单品
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午5:57:53
	 * 参       数:categoryId分类id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesByCategory(int page,int pageSize,String categoryId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join category_image as ci on ci.image_id=i.id " +
				" where ci.category_id=? and type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,categoryId,Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取某一分类标签组下的单品
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午6:10:42
	 * 参       数:标签组id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesByLabelGroup(int page,int pageSize,String labelGroupId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join label_group_image as lgi on lgi.image_id=i.id " +
				" where lgi.label_group_id=? and type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取某一标签下的单品
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午6:21:07
	 * 参       数:labelId标签id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesByLabel(int page,int pageSize,String labelId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join label_image as li on li.image_id=i.id " +
				" where li.label_id=? and type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,labelId,Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取标签组标签下的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午6:25:37
	 * 参       数:标签组id，标签id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesByLabelGroupLabel(int page,int pageSize,String labelGroupId,String labelId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join label_image as li on li.image_id=i.id " +
				" inner join label_group_image as lgi on lgi.image_id=i.id " +
				" inner join label_group_label as lgl on lgl.label_group_id=lgi.label_group_id " +
				" where lgl.label_group_id=? and lgl.label_id=? and lgl.label_id=li.label_id and type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,labelId,Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取分类标签下的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午6:41:05
	 * 参       数:分类id，标签id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesByCategoryLabel(int page,int pageSize,String categoryId,String labelId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join label_image as li on li.image_id=i.id " +
				" inner join category_image as ci on ci.image_id=i.id " +
				" inner join category_label as cl on cl.category_id=ci.category_id " +
				" where cl.category_id=? and cl.label_id=li.label_id  and  cl.label_id=? and type=? " +
				" order by rand()";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,categoryId,labelId,Image.ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：根据图片id获取单品详情页信息
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 上午11:46:38
	 * 参       数:图片id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 * @return 
	 */
	public static Image getSingle(String id){
		return dao.findFirst("select i.id,i.description,i.check_time,i.detail_url,i.detail_width,i.detail_height,i.price,i.buy_url,i.source_id,u.avatar_large,u.nickname,i.user_id from image as i " +
				" inner join user as u on u.id=i.user_id " +
				" where i.id=? and i.check_state=? and i.type=?", id,ImageState.SHOW.ordinal(),ImageType.SINGLE.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获取主题模板
	 * 作        者：尹东东
	 * 创建时间：2013-6-5 上午11:21:58
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getTemplet(int page,int pageSize,String labelGroupId,String labelId){
		String select = "select i.id,i.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "";
		Page<Image> imgs = null;
		if(StringUtils.isEmpty(labelId)){
			sqlExceptSelect = "from image as i " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" where lgi.label_group_id=? " +
					" order by rand()";
			imgs = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId);
		}else {
			sqlExceptSelect = "from image as i " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" inner join label_image as li on  li.image_id=i.id " +
					" inner join label_group_label as lgl on lgl.label_group_id=lgi.label_group_id and lgl.label_id=li.label_id " +
					" where lgl.label_group_id=? and lgl.label_id=? " +
					" order by rand()";
			imgs = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,labelId);
		}
		return imgs;
	}
	
	/**
	 * 功能描述：图片类型：0单品、1明星、2主题模板、3街拍达人、4单品组合、5主题单品组合
	 * 作        者：尹东东 
	 * 创建时间：2013-5-14 上午10:38:29
	 * 版  本  号：1.0
	 */
	public enum ImageType {
		SINGLE,STAR,THEME,DAREN,SINGLE_COMPOSE,THEME_SINGLE_COMPOSE
	}
	
	/**
	 * 功能描述：审核状态：0待审核、1显示、2隐藏（自己可见）、3删除（自己也不可见，等待被系统删除）、4待发布(审核通过后进入待发布状态) 
	 * 作        者：尹东东 
	 * 创建时间：2013-5-14 上午11:08:09
	 * 版  本  号：1.0
	 */
	public enum ImageState {
		NOT_CHECK,SHOW,HIDE,DELETE,AWAIT 
	}
	
	/*******************************************审核********************************************************/
	
	/**
	 * 
	 * 功能描述：获取所有图片按图片状态和图片类型查询
	 * 作        者：尹东东
	 * 创建时间：2013-6-10 上午11:55:31
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getImagesByState(int state,int type,int page,int pageSize){
		String select = "select i.id,i.description,i.url,i.type,i.check_state";
		String sqlExceptSelect = "from image as i " +
				" where i.check_state=? and i.type=? " +
				" order by i.create_time desc";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,state,type);
	}
	
	/**
	 * 
	 * 功能描述：批量提交需要审核的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-10 下午7:00:25
	 * 参       数:images
	 * 返       回:返回处理成功的数量
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static int[] checkedImages(List<Image> images){
		String sql = "update image set check_state=?,check_time=CURRENT_TIMESTAMP() where id=?";
		return Db.batch(sql, "check_state,id", images, 10);
	}
	
	/**
	 * 
	 * 功能描述：获取不在单品相似表里的单品数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-11 上午11:53:33
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Image> getSinglesNoSimilarItems(int page,int pageSize){
		String select = "select i.id,i.description,i.url,i.type";
		String sqlExceptSelect = "from image as i " +
				" where i.check_state=? and i.type=? and i.id not in (select si.image_id from similar_items as si) " +
				" order by i.create_time desc";
		return dao.paginate(page, pageSize, select, sqlExceptSelect,ImageState.SHOW.ordinal(),ImageType.SINGLE.ordinal());
	}
}