/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.aimeili;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.koorz.modules.crawl.CrawlHistory;
import com.koorz.modules.crawl.http.SimpleHttpClient;
import com.koorz.modules.crawl.parser.AbstractTask;
import com.koorz.modules.crawl.parser.TaskService;
import com.koorz.modules.crawl.parser.UrlWrapper;
import com.koorz.modules.image.Image;
import com.koorz.utils.DateUtil;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午10:36:24
 * 版  本  号：1.0
 */
public class AimeiliTask extends AbstractTask {
	private JSONArray jsonArray = null;
	private TaskService taskService = TaskService.getInstance();
	
	@Override
	public boolean execute() {
		UrlWrapper navi = getUrl();
		if(navi != null){
			SimpleHttpClient client = new SimpleHttpClient();
			String result = client.get(navi.getUrl(),getCookie());
			try {
				JSONObject json = JSON.parseObject(result);
				JSONObject data = json.getJSONObject("data");
				jsonArray = data.getJSONArray("templates");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("AimeiliTask 获取数据失败！"+navi.getUrl(), e);
				client.close();
				return false;
			}
			client.close();
			return true;
		}
		return false;
	}

	@Override
	public void parser() {
		if(jsonArray != null){
			SimpleHttpClient client = new SimpleHttpClient();
			for(int i=0;i<jsonArray.size();i++){
				JSONObject content = jsonArray.getJSONObject(i);
				CrawlHistory crawl = CrawlHistory.dao.findFirst("select * from crawl_history where url=?", content.getString("bg_image"));
				if(crawl != null){
					continue;
				}
				//保存主图
				Image image = new Image();
				String url = content.getString("bg_image");
				image.set("description", content.getString("template_name"));
				image.set("url", url);
				image.set("check_state", Image.ImageState.SHOW.ordinal());
				image.set("check_time", DateUtil.getNowDateTime());
				image.set("type", Image.ImageType.THEME.ordinal());
				image.set("source_id", content.getString("_id"));
				image.set("user_id", "100000");
				image = taskService.saveImage(image);
				
				taskService.addImageToLabel(image.getStr("id"), getUrl().getLabel());
				taskService.addImageToCategory(image.getStr("id"), getUrl().getCategoryId());
				taskService.addImageToLabelGroup(image.getStr("id"), getUrl().getLabelGroupId());
				
				///下载主图图片
				try {
					taskService.compressImage(client, image);
					
					String host = StringUtils.substringBetween(url, "http://", "/");
					CrawlHistory crawlH = new CrawlHistory();
					crawlH.set("website", host);
					crawlH.set("url", url);
					taskService.addCrawlHistory(crawlH);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("保存图片失败 compressImage,url:" + url, e);
				}
			}
			client.close();
		}
	}

}
