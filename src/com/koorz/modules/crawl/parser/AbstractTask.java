/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;

import javax.servlet.http.Cookie;

import com.jfinal.log.Logger;

/**
 * 功能描述：要处理的数据任务
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午8:34:41
 * 版  本  号：1.0
 */
public abstract class AbstractTask {
	protected final Logger logger = Logger.getLogger(getClass());
	
	//任务名称
	private String name;
	
	//请求url
	private UrlWrapper url;
	
	private String cookie;
	
	private Cookie[] cookies;
	
	/**
	 * 
	 * 功能描述：请求数据成功后把需要的数据验证好后传递给parser处理
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午8:40:46
	 * 参       数:
	 * 返       回:执行成功，返回true，失败返回false；在成功时才会继续执行parser（）方法
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public abstract boolean execute();
	
	/**
	 * 
	 * 功能描述：保存数据处理
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午8:40:51
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public abstract void parser();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UrlWrapper getUrl() {
		return url;
	}

	public void setUrl(UrlWrapper url) {
		this.url = url;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public Cookie[] getCookies() {
		return cookies;
	}

	public void setCookies(Cookie[] cookies) {
		this.cookies = cookies;
	}
	
	
}
