/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.koorz.modules.crawl.aimeili.AimeiliParser;


/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午8:12:10
 * 版  本  号：1.0
 */
public class CrawlFactory {
	private static class SingletonHolder { 
		static final CrawlFactory INSTANCE = new CrawlFactory(); 
	}
	
	public static CrawlFactory getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	private CrawlFactory() {}
	
	private final List<AbstractTask> tasks = Collections.synchronizedList(new ArrayList<AbstractTask>());
	
	private final List<TaskProcess> process = Collections.synchronizedList(new ArrayList<TaskProcess>());
	
	/**
	 * 
	 * 功能描述：创建解析器
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午9:52:28
	 * 参       数:type 解析器类型
	 * 返       回:IParser
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public IParser createParser(CrawlType type){
		IParser parser = null;
		switch (type) {
		case HICHAO:
			
			break;
		case AIMEILI:
			parser = new AimeiliParser();	
			break;
		case MEILISHUO:
			
			break;
		case MOGUJIE:
			
			break;
		case TUOLAR:
			
			break;
		case TAOKE:
			
			break;
		default:
			break;
		}
		return null;
	}
	
	/**
	 * 
	 * 功能描述：获取要处理的任务
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午8:26:45
	 * 参       数:无
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public AbstractTask getTask(){
		if (!tasks.isEmpty()) {
			return tasks.remove(0);
		}
		return null;
	}
	
	/**
	 * 
	 * 功能描述：添加任务
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午8:58:12
	 * 参       数:task
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void addTask(AbstractTask task){
		tasks.add(task);
	}
	
	/**
	 * 
	 * 功能描述：没有任务了移除TaskProcess
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午9:36:03
	 * 参       数:p
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public boolean removeProcess(TaskProcess taskProcess){
		if (!process.isEmpty()) {
			return process.remove(taskProcess);
		}
		return true;
	}
	
	/**
	 * 
	 * 功能描述：添加处理器
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午9:37:56
	 * 参       数:taskProcess
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void addProcess(TaskProcess taskProcess){
		process.add(taskProcess);
	}
	
	/**
	 * 
	 * 功能描述：获取当前需要处理的任务数
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午9:40:40
	 * 参       数:
	 * 返       回:int
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public int getTasksSize(){
		return tasks.size();
	}
	
	/**
	 * 
	 * 功能描述：获取当前在处理任务的处理器数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-9 下午9:41:12
	 * 参       数:
	 * 返       回:int
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public int getProcessSize(){
		return process.size();
	}
}
