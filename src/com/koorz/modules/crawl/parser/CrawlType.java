/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;
/**
 * 功能描述：抓取网站类型
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午4:06:46
 * 版  本  号：1.0
 */
public enum CrawlType {
	//星衣橱、爱美丽、美丽说、蘑菇街、拖拉网、淘宝客
	HICHAO,AIMEILI,MEILISHUO,MOGUJIE,TUOLAR,TAOKE
}
