/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl.parser;
/**
 * 功能描述：任务处理器
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午8:23:28
 * 版  本  号：1.0
 */
public class TaskProcess implements Runnable{
	
	//重试5次后结束当前线程
	private int retry = 1;
	
	@Override
	public void run() {
		CrawlFactory factory = CrawlFactory.getInstance();
		while (true) {
			AbstractTask task = factory.getTask();
			if(task != null){
				if(task.execute())
				task.parser();
			}else if(retry > 5){
				break;
			}
			retry++;
		}
	}
}
