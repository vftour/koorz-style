/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.crawl;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：要抓取的网站
 * 作        者：尹东东 
 * 创建时间：2013-6-9 下午7:45:28
 * 版  本  号：1.0
 */
public class CrawlWebsite extends Model<CrawlWebsite>{
	private static final long serialVersionUID = -8723427363426697307L;
	public static final CrawlWebsite dao = new CrawlWebsite();
	
	public static void saveCrawlWebsite(CrawlWebsite c){
		c.set("id", IdManage.nextId(IdManage.CRAWL_WEBSITE)).save();
	}
}
