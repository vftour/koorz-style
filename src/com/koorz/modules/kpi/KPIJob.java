/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.kpi;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.koorz.modules.commission.VirtualAccount;
import com.koorz.modules.commission.VirtualCurrency;
import com.koorz.modules.user.User;

/**
 * 功能描述：kpi考核结算任务；
 * 作        者：尹东东 
 * 创建时间：2013-6-14 上午9:00:58
 * 版  本  号：1.0
 */
public class KPIJob implements Runnable {

	@Override
	public void run() {
		/**
		 * 1、先查出用户、
		 * 2、按照用户查出订单
		 * 3、计算一个用户交易佣金之和；
		 */
		int page = 1;
		int pageSize = 10;
		while (true) {
			Page<User> userPage = getUsers(page,pageSize);
			if(userPage.getList().size() > 0){
				for(User user : userPage.getList()){
					//总佣金
					double total = getAmount(user.getStr("id"));
					//用户所得
					double userCommission = total*user.getDouble("commission_rate");
					//系统所得
					double systemCommission = total-userCommission;
					
					//计算奖励
					RewardStandards standards = getStandards(total);
					if(standards != null){
						RewardRecord rr = new RewardRecord();
						//奖励金额
						double money = total*standards.getDouble("reward_rate");
						rr.set("money", money);
						rr.set("standards_id", standards.getStr("id"));
						rr.set("standards_name", standards.getStr("name"));
						rr.set("user_id", user.getStr("id"));
						rr.set("amount", total);
						RewardRecord.saveRewardRecord(rr);
						
						User.updateBalance(user, money+userCommission);
						
						//收入
						VirtualAccount incomAccount = new VirtualAccount();
						incomAccount.set("money", systemCommission);
						incomAccount.set("type", VirtualAccount.Type.PERCENTAGE.ordinal());
						VirtualAccount.saveVirtualAccount(incomAccount);
						
						//奖励支出
						VirtualAccount rewardAccount = new VirtualAccount();
						rewardAccount.set("money", -money);
						rewardAccount.set("type", VirtualAccount.Type.REWARD.ordinal());
						VirtualAccount.saveVirtualAccount(rewardAccount);
					}else {
						User.updateBalance(user,userCommission);
						//收入
						VirtualAccount incomAccount = new VirtualAccount();
						incomAccount.set("money", systemCommission);
						incomAccount.set("type", VirtualAccount.Type.PERCENTAGE.ordinal());
						VirtualAccount.saveVirtualAccount(incomAccount);
					}
					//记录总账,每次结算的用户的佣金总数都要记入总账
					VirtualCurrency vc = new VirtualCurrency();
					vc.set("money", total);
					VirtualCurrency.saveVirtualCurrency(vc);
				}
			}else {
				break;
			}
		}
	}

	/**
	 * 
	 * 功能描述：获取用户除管理员外
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 下午6:48:21
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private Page<User> getUsers(int page,int pageSize){
		String select = "select id,account_balance,commission_rate,type";
		String sqlExceptSelect = "from user " +
				" where type != ? " +
				" order by create_time desc";
		return User.dao.paginate(page, pageSize, select, sqlExceptSelect,User.UserType.ADMIN.ordinal());
	}
	
	/**
	 * 
	 * 功能描述：获得用户的总成交额（这个月内的佣金总和）
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 上午10:50:43
	 * 参       数:用户id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private double getAmount(String uid){
		String select = "select SUM(real_pay_fee*commission_rate) as total from order_form " +
				" where user_id=? and DATE_FORMAT(pay_time,'%Y-%m-%d') > DATE_ADD(DATE_ADD(CURDATE(), INTERVAL '-1' DAY), INTERVAL '-1' MONTH) and DATE_FORMAT(pay_time,'%Y-%m-%d') <= DATE_ADD(CURDATE(), INTERVAL '-1' DAY) " +
				" order by pay_time desc";
		Record total = Db.findFirst(select,uid);
		return total.getDouble("total") == null?0.0:total.getDouble("total");
	}
	
	/**
	 * 
	 * 功能描述：获得达到的指标
	 * 作        者：尹东东
	 * 创建时间：2013-6-14 上午11:48:17
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private RewardStandards getStandards(double total){
		List<RewardStandards> standards = RewardStandards.getRewardStandardsList();
		Collections.sort(standards, new Comparator<RewardStandards>() {
			@Override
			public int compare(RewardStandards o1, RewardStandards o2) {
				return o1.getDouble("amount").compareTo(o2.getDouble("amount"));
			}
		});
		for(int i=0;i<standards.size()-1;i++){
			RewardStandards pre = standards.get(i);
			RewardStandards nex = standards.get(i+1);
			if(total == nex.getDouble("amount")){
				return nex;
			}else if(total >= pre.getDouble("amount") && total < nex.getDouble("amount")){
				return pre;
			}
		}
		return null;
	}
}
