/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.catalog;

import org.apache.commons.lang.StringUtils;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：标签组实体
 * 作        者：尹东东 
 * 创建时间：2013-5-23 下午8:51:46
 * 版  本  号：1.0
 */
public class LabelGroup extends Model<LabelGroup> {
	private static final long serialVersionUID = 3206058539399962775L;
	public static final LabelGroup dao = new LabelGroup();
	
	public static void saveLabelGroup(LabelGroup l){
		l.set("id", IdManage.nextId(IdManage.LABEL_GROUP)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	public static LabelGroup getLabelGroup(String[] columns,String[] vals){
		StringBuilder sb = new StringBuilder("select * from label_group where");
		for(int i=0;i<columns.length;i++){
			sb.append(" "+columns[i]+ "='"+vals[i] +"' and");
		}
		return dao.findFirst(StringUtils.substringBeforeLast(sb.toString(), " and"));
	}
}
