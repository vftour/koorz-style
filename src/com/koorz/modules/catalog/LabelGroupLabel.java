/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.catalog;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：标签组下的标签实体
 * 作        者：尹东东 
 * 创建时间：2013-5-23 下午8:55:37
 * 版  本  号：1.0
 */
public class LabelGroupLabel extends Model<LabelGroupLabel> {
	private static final long serialVersionUID = -3126194129953895071L;
	public static final LabelGroupLabel dao = new LabelGroupLabel();
	
	public static void saveLabelGroupLabel(LabelGroupLabel l){
		l.set("id", IdManage.nextId(IdManage.LABEL_GROUP_LABEL)).save();
	}
	
	public static LabelGroupLabel getLabelGroupLabel(String labelGroupId,String labelId){
		return dao.findFirst("select * from label_group_label where label_group_id=? and label_id=?",labelGroupId,labelId);
	}
	
	/**
	 * 
	 * 功能描述：保存多个标签到标签组
	 * 作        者：尹东东
	 * 创建时间：2013-6-15 下午11:50:43
	 * 参       数:标签组id，labels多个标签id数组
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void saveLabelsToLabelGroup(String labelGroupId,String[] labels){
		for(String id:labels){
			LabelGroupLabel lgl = getLabelGroupLabel(labelGroupId,id);
			if(lgl == null){
				lgl = new LabelGroupLabel();
				lgl.set("label_group_id", labelGroupId);
				lgl.set("label_id", id);
				lgl.set("hot", 0);
				saveLabelGroupLabel(lgl);
			}
		}
	}
	
	public static int[] batchUpdate(List<LabelGroupLabel> labels){
		String sql = "update label_group_label set hot=? where id=?";
		return Db.batch(sql, "hot,id", labels, 20);
	}
}
