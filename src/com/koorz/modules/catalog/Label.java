/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.catalog;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：标签实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-5-12 下午11:09:06
 * 版  本  号：1.0
 */
public class Label extends Model<Label>{
	private static final long serialVersionUID = 1679511844906090411L;
	public static final Label dao = new Label();
	
	public static void saveLabel(Label l){
		l.set("id", IdManage.nextId(IdManage.LABEL)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：根据指定列获取标签
	 * 作        者：尹东东
	 * 创建时间：2013-5-14 上午11:00:19
	 * 参       数：column表字段，val值
	 * 返       回:Label
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Label getLabel(String column,String val){
		return dao.findFirst("select * from label where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：将新的标签名批量添加到标签
	 * 作        者：尹东东
	 * 创建时间：2013-6-15 下午8:48:37
	 * 参       数:标签名数组,审核状态
	 * 返       回:返回保存后的标签
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<Label> batchAddLabel(String[] names,int status){
		String sql = "insert into label(id, name,hot,create_time,check_state) values(?,?,?,?,?)";
		List<Label> list = new ArrayList<Label>();
		for(String name:names){
			Label label = getLabel("name",name);
			if(label == null){
				label = new Label();
				label.set("id", IdManage.nextId(IdManage.LABEL));
				label.set("name", name);
				label.set("hot", 0);
				label.set("create_time", DateUtil.getNowDateTime());
				label.set("check_state", status);
				list.add(label);
			}
		}
		Db.batch(sql, "id,name,hot,create_time,check_state", list, 20);
		return list;
	}
	
	/**
	 * 
	 * 功能描述：批量修改标签
	 * 作        者：尹东东
	 * 创建时间：2013-6-16 上午11:01:36
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static int[] batchUpdate(List<Label> labels){
		String sql = "update label set check_state=?,hot=?,name=? where id=?";
		return Db.batch(sql, "check_state,hot,name,id", labels, 20);
	}
	
	public static int[] batchDelete(List<Label> labels){
		String sql = "delete from label where id=?";
		return Db.batch(sql, "id", labels, 20);
	}
	
	public static int[] batchDelete(String[] labels){
		String sql = "delete from label where id=?";
		return Db.batch(sql, new Object[][]{labels}, 20);
	}
	
	/**
	 * 功能描述：审核状态：0待审核、1显示、2隐藏
	 * 作        者：尹东东 
	 * 创建时间：2013-5-14 上午11:08:09
	 * 版  本  号：1.0
	 */
	public enum LabelState {
		NOT_CHECK,SHOW,HIDE
	}
}
