/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.catalog;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：分类下的标签实体
 * 作        者：尹东东 
 * 创建时间：2013-5-25 下午2:25:17
 * 版  本  号：1.0
 */
public class CategoryLabel extends Model<CategoryLabel> {
	private static final long serialVersionUID = -5432646526393491732L;
	public static final CategoryLabel dao = new CategoryLabel();
	
	public static void saveCategoryLabel(CategoryLabel cl){
		cl.set("id", IdManage.nextId(IdManage.CATEGORY_LABEL)).save();
	}
	
	public static CategoryLabel getCategoryLabel(String cid,String lid){
		return dao.findFirst("select * from category_label where category_id=? and label_id=?",cid,lid);
	}
	
	/**
	 * 
	 * 功能描述：保存多个标签到分类
	 * 作        者：尹东东
	 * 创建时间：2013-5-26 下午3:48:04
	 * 参       数:cid分类id，labels多个标签id数组
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void saveLabelsToCategory(String cid,String[] labels){
		for(String id:labels){
			CategoryLabel cl = getCategoryLabel(cid,id);
			if(cl == null){
				cl = new CategoryLabel();
				cl.set("category_id", cid);
				cl.set("label_id", id);
				cl.set("hot", 0);
				saveCategoryLabel(cl);
			}
		}
	}
	
	/**
	 * 
	 * 功能描述：保存多个标签到分类
	 * 作        者：尹东东
	 * 创建时间：2013-5-26 下午3:48:04
	 * 参       数:cid分类id，labels多个标签id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static void saveLabelsToCategory(String cid,List<String> labels){
		for(String id:labels){
			CategoryLabel cl = getCategoryLabel(cid,id);
			if(cl == null){
				cl = new CategoryLabel();
				cl.set("category_id", cid);
				cl.set("label_id", id);
				cl.set("hot", 0);
				saveCategoryLabel(cl);
			}
		}
	}
}