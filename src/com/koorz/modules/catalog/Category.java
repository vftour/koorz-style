/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.catalog;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：分类实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-5-12 下午11:08:26
 * 版  本  号：1.0
 */
public class Category extends Model<Category>{
	private static final long serialVersionUID = 2000324263522106106L;
	public static final Category dao = new Category();
	
	public static void saveCategory(Category c){
		c.set("id", IdManage.nextId(IdManage.CATEGORY)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：根据指定列获取分类
	 * 作        者：尹东东
	 * 创建时间：2013-5-13 下午11:15:17
	 * 参       数：column表字段，val值
	 * 返       回:Category
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Category getCategory(String column,String val){
		return dao.findFirst("select * from category where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：获取所有分类
	 * 作        者：尹东东
	 * 创建时间：2013-6-15 上午10:44:07
	 * 参       数:
	 * 返       回:List<Category>
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<Category> getCategoryList(){
		return dao.find("select * from category");
	}
	
	/**
	 * 
	 * 功能描述：获取当前分类下的标签
	 * 作        者：尹东东
	 * 创建时间：2013-5-26 下午3:22:31
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public List<Label> getLabels(){
		return Label.dao.find("select l.* from label as l " +
				" inner join category_label as cl on cl.label_id=l.id " +
				" where cl.category_id=? order by cl.hot desc", getStr("id"));
	}
}
