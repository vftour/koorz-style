/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.style;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-30 下午9:19:38
 * 版  本  号：1.0
 */
public class SimilarStyles extends Model<SimilarStyles> {
	private static final long serialVersionUID = 1366756991937465936L;
	public static final SimilarStyles dao = new SimilarStyles();
	
	public static void saveSimilarStyles(SimilarStyles s){
		s.set("id", IdManage.nextId(IdManage.SIMILAR_STYLES)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：获取搭配相似搭配
	 * 作        者：尹东东
	 * 创建时间：2013-5-30 下午10:08:55
	 * 参       数:styleId搭配id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<SimilarStyles> getSimilarStyles(String styleId){
		String select = "select s.id,s.description,i.similar_url";
		String sqlExceptSelect = "from image as i " +
				" inner join style as s on s.image_id=i.id " +
				" inner join similar_styles as ss on ss.item_id=s.id " +
				" where ss.style_id=? " +
				" order by rand()";
		return dao.paginate(1, 6, select, sqlExceptSelect, styleId);
	}
}
