/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.style;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：搭配实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-5-12 下午11:09:56
 * 版  本  号：1.0
 */
public class Style extends Model<Style>{
	private static final long serialVersionUID = -4216602617975201996L;
	public static final Style dao = new Style();
	
	public static void saveStyle(Style s){
		s.set("id", IdManage.nextId(IdManage.STYLE)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	public static Style getStyle(String column,String val){
		return dao.findFirst("select * from style where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：获取所有搭配下的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午8:00:21
	 * 参       数:搭配类型 ：0、单品组合搭配；1、明星/街拍达人搭配；2、主题搭配   type=-1时表示所有
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getStyles(int page,int pageSize,int type){
		String select = "select s.id,s.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "";
		Page<Style> styles = null;
		if(type == -1){
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect);
		}else {
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" where i.type=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,type);
		}
		
		for(Style style:styles.getList()){
			style.put("style_items", Db.find("select ci.category_id from style_items as si " +
					" inner join category_image as ci on ci.image_id=si.image_id " +
					" where si.style_id=?", style.getStr("id")));
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：获取某一用户的搭配数据
	 * 作        者：尹东东
	 * 创建时间：2013-5-29 下午9:58:01
	 * 参       数:uid用户id
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getUserStyles(int page,int pageSize,String uid){
		String select = "select s.id,s.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "from image as i " +
				" inner join style as s on s.image_id=i.id " +
				" inner join user as u on u.id=s.user_id " +
				" where s.check_state=? and u.id=? " +
				" order by rand()";
		Page<Style> styles = dao.paginate(page, pageSize, select, sqlExceptSelect,StyleState.SHOW.ordinal(),uid);
		for(Style style:styles.getList()){
			style.put("style_items", Db.find("select ci.category_id from style_items as si " +
					" inner join category_image as ci on ci.image_id=si.image_id " +
					" where si.style_id=?", style.getStr("id")));
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：获取某一标签组下的搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午7:22:44
	 * 参       数:标签组id  搭配类型 ：0、单品组合搭配；1、明星/街拍达人搭配；2、主题搭配   type=-1时表示所有
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getStylesByLabelGroup(int page,int pageSize,String labelGroupId,int type){
		String select = "select s.id,s.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "";
		Page<Style> styles = null;
		if(type == -1){
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" where lgi.label_group_id=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId);
		}else {
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" where lgi.label_group_id=? and i.type=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,type);
		}
		
		for(Style style:styles.getList()){
			style.put("style_items", Db.find("select ci.category_id from style_items as si " +
					" inner join category_image as ci on ci.image_id=si.image_id " +
					" where si.style_id=?", style.getStr("id")));
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：获取标签组标签下的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午7:20:12
	 * 参       数:标签组id，标签id  搭配类型 ：0、单品组合搭配；1、明星/街拍达人搭配；2、主题搭配   type=-1时表示所有
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getStylesByLabelGroupLabel(int page,int pageSize,String labelGroupId,String labelId,int type){
		String select = "select s.id,s.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		
		String sqlExceptSelect = "";
		Page<Style> styles = null;
		if(type == -1){
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_image as li on li.image_id=i.id " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" inner join label_group_label as lgl on lgl.label_group_id=lgi.label_group_id " +
					" where lgl.label_group_id=? and lgl.label_id=? and lgl.label_id=li.label_id " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,labelId);
		}else {
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_image as li on li.image_id=i.id " +
					" inner join label_group_image as lgi on lgi.image_id=i.id " +
					" inner join label_group_label as lgl on lgl.label_group_id=lgi.label_group_id " +
					" where lgl.label_group_id=? and lgl.label_id=? and lgl.label_id=li.label_id and i.type=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelGroupId,labelId,type);
		}
		
		for(Style style:styles.getList()){
			style.put("style_items", Db.find("select ci.category_id from style_items as si " +
					" inner join category_image as ci on ci.image_id=si.image_id " +
					" where si.style_id=?", style.getStr("id")));
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：获取某一标签下的搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-4 下午7:28:37
	 * 参       数:labelId标签id  搭配类型 ：0、单品组合搭配；1、明星/街拍达人搭配；2、主题搭配   type=-1时表示所有
	 * 返       回:用于瀑布流显示的数据
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getStylesByLabel(int page,int pageSize,String labelId,int type){
		String select = "select s.id,s.description,i.waterfall_url,i.type,i.price,i.source,i.waterfall_width,i.waterfall_height";
		String sqlExceptSelect = "";
		Page<Style> styles = null;
		if(type == -1){
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_image as li on li.image_id=i.id " +
					" where li.label_id=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelId);
		}else {
			sqlExceptSelect = "from style as s " +
					" inner join image as i on i.id=s.image_id " +
					" inner join label_image as li on li.image_id=i.id " +
					" where li.label_id=? and i.type=? " +
					" order by rand()";
			styles = dao.paginate(page, pageSize, select, sqlExceptSelect,labelId,type);
		}
		for(Style style:styles.getList()){
			style.put("style_items", Db.find("select ci.category_id from style_items as si " +
					" inner join category_image as ci on ci.image_id=si.image_id " +
					" where si.style_id=?", style.getStr("id")));
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：根据搭配id获取搭配详情页信息
	 * 作        者：尹东东
	 * 创建时间：2013-5-28 下午6:27:46
	 * 参       数:搭配id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Style getStyle(String id){
		Style style = dao.findFirst("select s.id,s.description,s.check_time,i.detail_url,i.detail_width,i.detail_height,u.avatar_large,u.nickname,s.user_id from style as s " +
				" inner join image as i on i.id=s.image_id " +
				" inner join user as u on u.id=s.user_id " +
				" where s.id=? and s.check_state=?", id,StyleState.SHOW.ordinal());
		//获取搭配项
		style.put("styleItems", StyleItems.getStyleItems(id));
		return style;
	}
	
	/**
	 * 功能描述：审核状态：0待审核、1显示、2隐藏、3删除
	 * 作        者：尹东东 
	 * 创建时间：2013-5-14 下午10:21:41
	 * 版  本  号：1.0
	 */
	public enum StyleState {
		NOT_CHECK,SHOW,HIDE,DELETE
	}
	
	/**
	 * 功能描述：搭配类型 ：0、单品组合搭配；1、明星/街拍达人搭配；2、主题搭配
	 * 作        者：尹东东 
	 * 创建时间：2013-6-2 下午3:56:20
	 * 版  本  号：1.0
	 */
	public enum StyleType {
		SINGLE,STAR,THEME
	}
	
	/**
	 * 
	 * 功能描述：获取审核数据
	 * 作        者：尹东东
	 * 创建时间：2013-5-22 下午9:23:46
	 * 参       数:page页数、pageSize每页大小、type搭配类型：0单品、1搭配、2主题模板；、filter过滤
	 * 返       回:Page<Style>
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static Page<Style> getStylesByState(int state,int type,int page,int pageSize){
		String select = "select s.id,s.check_state,s.description,i.url";
		String sqlExceptSelect = "from style as s " +
				" inner join image as i on i.id=s.image_id " +
				" where s.check_state=? and i.type=? " +
				" order by s.create_time desc";
		Page<Style> styles = dao.paginate(page, pageSize, select, sqlExceptSelect,state,type);
		for(Style style : styles.getList()){
			List<Record> list = Db.find("select s.id,s.sequence,s.style_id,s.image_id,i.style_url from style_items as s " +
					" inner join image as i on i.id=s.image_id " +
					" where s.style_id=? order by s.sequence asc", style.getStr("id"));
			style.put("items", list);
		}
		return styles;
	}
	
	/**
	 * 
	 * 功能描述：批量提交需要审核的数据
	 * 作        者：尹东东
	 * 创建时间：2013-6-10 下午7:00:25
	 * 参       数:images
	 * 返       回:返回处理成功的数量
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static int[] checkedStyles(List<Style> styles){
		String sql = "update style set check_state=?,description=?,check_time=CURRENT_TIMESTAMP() where id=?";
		return Db.batch(sql, "check_state,description,id", styles, 10);
	}
}
