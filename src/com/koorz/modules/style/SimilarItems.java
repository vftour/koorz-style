/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.style;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：相似单品
 * 作        者：尹东东 
 * 创建时间：2013-5-18 上午11:50:02
 * 版  本  号：1.0
 */
public class SimilarItems extends Model<SimilarItems>{
	private static final long serialVersionUID = -2673078029301234219L;
	public static final SimilarItems dao = new SimilarItems();
	
	public static void saveSimilarItems(SimilarItems s){
		s.set("id", IdManage.nextId(IdManage.SILIMAR_ITEMS)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	/**
	 * 
	 * 功能描述：获取相似单品列表
	 * 作        者：尹东东
	 * 创建时间：2013-5-18 上午11:51:29
	 * 参       数:搭配项单品图片id
	 * 返       回:SimilarItems单品项列表
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public static List<SimilarItems> getSimilarItems(String id){
		return dao.find("select i.item_url,i.item_height,i.item_width,i.buy_url,i.price,i.source_id,i.description from similar_items as si " +
				" inner join image as i on si.item_id=i.id " +
				" where si.image_id=? " +
				" order by si.sequence asc;", id);
	}
}
