/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-6-7 下午4:09:44
 * 版  本  号：1.0
 */
public abstract class AbstractParser implements IParser{
	private String cookie = "";
	
	public List<UrlWrapper> navis = new ArrayList<UrlWrapper>();

	/**
	 * 抓取数据数量
	 */
	private int crawlCount = 0;
	/**
	 * 最大抓取数量
	 */
	public int maxCount = Integer.MAX_VALUE;
	
	/**
	 * 进入图片队列的总数
	 */
	private int imageQueueTotal = 0;
	
	/**
	 * 图片队列已处理数
	 */
	private int imageQueueUsed = 0;
	
	public synchronized UrlWrapper getNavi() {
		if (!navis.isEmpty()) {
			return navis.remove(0);
		}
		return null;
	}
	
	public synchronized boolean isStopCrawl(){
		return (crawlCount + 1) > maxCount?true:false;
	}
	
	/**
	 * 
	 * 功能描述：记录已抓取数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-8 上午9:36:42
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public synchronized void setCrawlCount(){
		crawlCount++;
	}
	
	public synchronized void pushQueue(){
		imageQueueTotal++;
	}
	
	public synchronized void getQueue(){
		imageQueueUsed++;
	}
	
	/**
	 * 
	 * 功能描述：获取队列中当前还剩的数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午2:31:59
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public synchronized int getQueueTotal(){
		return imageQueueTotal -imageQueueUsed;
	}

	@Override
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	@Override
	public String getCookie() {
		return cookie;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}
}
