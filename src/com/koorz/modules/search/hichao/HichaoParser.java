/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search.hichao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.jfinal.log.Logger;
import com.koorz.modules.catalog.Category;
import com.koorz.modules.catalog.CategoryLabel;
import com.koorz.modules.catalog.Label;
import com.koorz.modules.catalog.LabelGroup;
import com.koorz.modules.catalog.LabelGroupLabel;
import com.koorz.modules.search.AbstractParser;
import com.koorz.modules.search.UrlWrapper;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述： 作 者：尹东东 创建时间：2013-5-27 下午2:24:17 版 本 号：1.0
 */
public class HichaoParser extends AbstractParser {
	protected final Logger logger = Logger.getLogger(getClass());
	private static class SingletonHolder { 
		static final HichaoParser INSTANCE = new HichaoParser(); 
	}
	
	public static HichaoParser getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	public List<UrlWrapper> menus = new ArrayList<UrlWrapper>();
	
	public synchronized UrlWrapper getMenus() {
		if (!menus.isEmpty()) {
			return menus.remove(0);
		}
		return null;
	}

	public synchronized void addNavi(UrlWrapper url) {
		this.navis.add(url);
	}
	
	public final CountDownLatch end = new CountDownLatch(1);
	private boolean isSetMaxCount = false;
	public synchronized void setMaxCount() {
		if(!isSetMaxCount){
			if(this.maxCount == 0){
				this.setMaxCount(navis.size());
			}
			isSetMaxCount = true;
			end.countDown();
		}
	}
	
	private HichaoParser() {}
	
	public void init(){
		logger.debug("进入init ");
		String categoryName = "搭配";
		String labelGroupName = "风格";
		Category category = Category.getCategory("name", categoryName);
		LabelGroup labelGroup = LabelGroup.dao.findFirst("select * from label_group where category_id=? and name=?", category.getStr("id"),labelGroupName);
		String styles[] = {"欧美", "本土", "日韩", "达人","潮男"};
		List<String> labels = new ArrayList<String>();
		for(String style : styles){
			Label label = Label.getLabel("name", style);
			if(label == null){
				label = new Label();
				label.set("name", style);
				label.set("hot", 0);
				label.set("check_state", Label.LabelState.SHOW.ordinal());
				Label.saveLabel(label);
			}
			labels.add(label.getStr("id"));
			
			for (int i = 1; i < 8; i++) {
				for (int k = 0; k < 3; k++) {
					menus.add(new UrlWrapper("http://www.hichao.com/api/starCategory?page="
							+ i + "&query=" + style + "&more=" + k,categoryName,labelGroupName,style,category.getStr("id"),labelGroup.getStr("id"),label.getStr("id")));
				}
			}
		}
		//保存标签到分类
		CategoryLabel.saveLabelsToCategory(category.getStr("id"), labels);
		
		//保存标签到标签组
		int index = 1;
		for(String id:labels){
			LabelGroupLabel lgl = LabelGroupLabel.dao.findFirst("select * from label_group_label where label_group_id=? and label_id=?", labelGroup.getStr("id"),id);
			if(lgl == null){
				lgl = new LabelGroupLabel();
				lgl.set("id", IdManage.nextId(IdManage.LABEL_GROUP_LABEL));
				lgl.set("label_group_id", labelGroup.getStr("id"));
				lgl.set("label_id", id);
				lgl.set("hot", 0);
				lgl.set("create_time", DateUtil.getNowDateTime());
				lgl.save();
			}
			index++;
		}
		logger.debug("init完成");
	}
}
