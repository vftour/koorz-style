/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search;


/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-5-27 下午1:21:35
 * 版  本  号：1.0
 */
public interface IParser {

	/**
	 * 
	 * 功能描述：初始化请求链接
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午4:01:15
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void init();
	
	/**
	 * 
	 * 功能描述：在需要cookie请求的时候需要设置
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午4:02:35
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void setCookie(String cookie);
	
	public String getCookie();
	
	/**
	 * 
	 * 功能描述：设置最大抓取数量,默认为请求数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午4:31:22
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void setMaxCount(int maxCount);
	
	/**
	 * 
	 * 功能描述：获取请求链接
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午3:57:23
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public UrlWrapper getNavi();
	
	/**
	 * 
	 * 功能描述：判断是否停止抓取
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午3:57:54
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public boolean isStopCrawl();
	
	/**
	 * 
	 * 功能描述：记录图片下载队列中的数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午3:59:20
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void pushQueue();
	
	/**
	 * 
	 * 功能描述：记录已经处理的图片队列的数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午3:59:23
	 * 参       数:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void getQueue();
	
	/**
	 * 
	 * 功能描述：获取图片队列中当前还剩的数量
	 * 作        者：尹东东
	 * 创建时间：2013-6-7 下午2:31:59
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public int getQueueTotal();
	
}
