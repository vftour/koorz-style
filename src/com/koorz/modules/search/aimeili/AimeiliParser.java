/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.search.aimeili;

import java.util.ArrayList;
import java.util.List;

import com.koorz.modules.catalog.Category;
import com.koorz.modules.catalog.CategoryLabel;
import com.koorz.modules.catalog.Label;
import com.koorz.modules.catalog.LabelGroup;
import com.koorz.modules.catalog.LabelGroupLabel;
import com.koorz.modules.search.AbstractParser;
import com.koorz.modules.search.UrlWrapper;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-6-2 下午6:20:54
 * 版  本  号：1.0
 */
public class AimeiliParser extends AbstractParser{
	private static class SingletonHolder { 
		static final AimeiliParser INSTANCE = new AimeiliParser(); 
	}
	
	public static AimeiliParser getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	private AimeiliParser() {}
	
	public void init(){
		String categoryName = "搭配";
		String labelGroupName = "主题";
		Category category = Category.getCategory("name", categoryName);
		LabelGroup labelGroup = LabelGroup.dao.findFirst("select * from label_group where category_id=? and name=?", category.getStr("id"),labelGroupName);
		String styles[] = {"12","13","14","17","18","26","28"};
		List<String> labels = new ArrayList<String>();
		for(String style : styles){
			String labelName = "";
			if(style.equals("12")){
				labelName = "复古";
			}else if(style.equals("13")){
				labelName = "欧美";
			}else if(style.equals("14")){
				labelName = "甜美";
			}else if(style.equals("17")){
				labelName = "英伦";
			}else if(style.equals("18")){
				labelName = "性感";
			}else if(style.equals("26")){
				labelName = "日韩";
			}else {
				labelName = "中国风";
			}
			
			Label label = Label.getLabel("name", labelName);
			if(label == null){
				label = new Label();
				label.set("name", labelName);
				label.set("hot", 0);
				label.set("check_state", Label.LabelState.SHOW.ordinal());
				Label.saveLabel(label);
			}
			labels.add(label.getStr("id"));
			
			for (int i = 1; i < 8; i++) {
				navis.add(new UrlWrapper("http://aimeili.kokozu.net/ajax?f=getTemplateList&style="+style+"&sp="+i,categoryName,labelGroupName,labelName,category.getStr("id"),labelGroup.getStr("id"),label.getStr("id")));
			}
		}
		//保存标签到分类
		CategoryLabel.saveLabelsToCategory(category.getStr("id"), labels);
		
		//保存标签到标签组
		int index = 1;
		for(String id:labels){
			LabelGroupLabel lgl = LabelGroupLabel.dao.findFirst("select * from label_group_label where label_group_id=? and label_id=?", labelGroup.getStr("id"),id);
			if(lgl == null){
				lgl = new LabelGroupLabel();
				lgl.set("id", IdManage.nextId(IdManage.LABEL_GROUP_LABEL));
				lgl.set("label_group_id", labelGroup.getStr("id"));
				lgl.set("label_id", id);
				lgl.set("hot", 0);
				lgl.set("create_time", DateUtil.getNowDateTime());
				lgl.save();
			}
			index++;
		}
		setMaxCount(navis.size());
	}
}
