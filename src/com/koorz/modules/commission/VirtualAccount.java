/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.commission;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.DateUtil;
import com.koorz.utils.IdManage;

/**
 * 功能描述：虚拟账户  回收分成余额所得、提现所得、奖励支出；
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:05:11
 * 版  本  号：1.0
 */
public class VirtualAccount extends Model<VirtualAccount>{
	private static final long serialVersionUID = 5516993428057290009L;
	public static final VirtualAccount dao = new VirtualAccount();
	
	public static void saveVirtualAccount(VirtualAccount a){
		a.set("id", IdManage.nextId(IdManage.VIRTUAL_ACCOUNT)).set("create_time", DateUtil.getNowDateTime()).save();
	}
	
	
	
	/**
	 * 功能描述：回收类型：0表示分成所得、1表示提现所得、2表示奖励支出、3提现手续费所得
	 * 作        者：尹东东 
	 * 创建时间：2013-6-13 下午2:35:58
	 * 版  本  号：1.0
	 */
	public enum Type {
		PERCENTAGE,DRAW,REWARD,CHARGE 
	}
}
