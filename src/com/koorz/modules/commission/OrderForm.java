/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.commission;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：订单交易记录
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午8:59:36
 * 版  本  号：1.0
 */
public class OrderForm extends Model<OrderForm>{
	private static final long serialVersionUID = -1606839839258671993L;
	public static final OrderForm dao = new OrderForm();
	
	public static void saveOrderForm(OrderForm o){
		o.set("id", IdManage.nextId(IdManage.ORDER_FORM)).save();
	}
}
