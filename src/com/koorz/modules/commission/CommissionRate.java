/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.commission;

import com.jfinal.plugin.activerecord.Model;
import com.koorz.utils.IdManage;

/**
 * 功能描述：佣金分成比例
 * 作        者：尹东东 
 * 创建时间：2013-6-12 下午9:00:56
 * 版  本  号：1.0
 */
public class CommissionRate extends Model<CommissionRate>{
	private static final long serialVersionUID = -5016074807454421957L;
	public static final CommissionRate dao = new CommissionRate();
	
	public static void saveCommissionRate(CommissionRate c){
		c.set("id", IdManage.nextId(IdManage.COMMISSION_RATE)).save();
	}
	
	/**
	 * 功能描述：佣金分配类型 0表示默认
	 * 作        者：尹东东 
	 * 创建时间：2013-6-20 下午5:08:01
	 * 版  本  号：1.0
	 */
	public enum Type {
		DEFAULT
	}
}
