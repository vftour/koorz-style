/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.modules.menu;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;
import com.koorz.utils.IdManage;

/**
 * 功能描述：前台菜单实体及公共方法
 * 作        者：尹东东 
 * 创建时间：2013-5-13 上午12:18:14
 * 版  本  号：1.0
 */
public class Menu extends Model<Menu>{
	private static final long serialVersionUID = -8185673196861729479L;
	public static final Menu dao = new Menu();
	
	public static void saveMenu(Menu m){
		m.set("id", IdManage.nextId(IdManage.MENU)).save();
	}
	
	public static Menu getMenu(String column,String val){
		return dao.findFirst("select * from menu where "+column+"=?",val);
	}
	
	/**
	 * 
	 * 功能描述：获取所有菜单
	 * 作        者：尹东东
	 * 创建时间：2013-5-13 下午10:28:23
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public List<Menu> getMenus(){
		return CacheKit.get("cache-menu", "cache-menu-key", new IDataLoader(){
			@Override
			public Object load() {
				return getMenus("0");
			}
		});
	}
	
	/**
	 * 
	 * 功能描述：遍历出所有菜单及子菜单
	 * 作        者：尹东东
	 * 创建时间：2013-5-13 下午11:02:50
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	private List<Menu> getMenus(String parentId){
		List<Menu> menus = dao.find("select id,name,url,code from menu where parent_id=? order by sequence asc",parentId);
		if(menus != null && menus.size() > 0){
			for(Menu m:menus){
				List<Menu> childs = getMenus(m.getStr("id"));
				if(childs != null) m.put("child", childs);
			}
			return menus;
		}
		return null;
	}
}
