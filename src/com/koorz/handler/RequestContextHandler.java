/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.koorz.utils.SessionContext;

/**
 * 功能描述：全局上下文
 * 作        者：尹东东 
 * 创建时间：2013-5-22 上午10:31:05
 * 版  本  号：1.0
 */
public class RequestContextHandler extends Handler{

	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
		SessionContext.init(request, response);
		SessionContext.get().user();
		nextHandler.handle(target, request, response, isHandled);
	}

}
