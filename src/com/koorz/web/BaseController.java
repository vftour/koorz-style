/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.web;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import com.jfinal.core.Controller;
import com.jfinal.log.Logger;
import com.koorz.constant.GlobalConstant;
import com.koorz.modules.user.User;

/**
 * 功能描述：
 * 作        者：尹东东 
 * 创建时间：2013-1-5 下午10:51:32
 * 版  本  号：1.0
 */
public class BaseController extends Controller{
	protected final Logger logger = Logger.getLogger(getClass());
	protected int pageSize = 10;
	protected String tokenName = "koorz_form_token";
	
	/**
	 * 
	 * 功能描述：获取当前登录的普通用户
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午10:56:06
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected User getSessionUser() {
		return (User) getSession(GlobalConstant.USER_TOKEN);
	}
	
	/**
	 * 
	 * 功能描述：获取当前登录的管理员
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午10:57:17
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected User getSessionAdminUser() {
		return (User) getSession(GlobalConstant.ADMIN_TOKEN);
	}
	
	/**
	 * 
	 * 功能描述：获取保存在session中的值
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午10:58:02
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected Object getSession(String key) {
		return getSessionAttr(key);
	}
	
	/**
	 * 
	 * 功能描述：保存用户到session中
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:00:37
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void setSessionUser(User user) {
		setSession(GlobalConstant.USER_TOKEN,user);
	}
	
	/**
	 * 
	 * 功能描述：保存用户id到cookie中
	 * 作        者：尹东东
	 * 创建时间：2013-2-26 下午11:56:49
	 * 参       数：uid用户id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void setCookieUser(String uid) {
		setCookie(GlobalConstant.USER_TOKEN, uid, GlobalConstant.COOKIE_MAX_AGE, "/");
	}
	
	/**
	 * 
	 * 功能描述：保存管理员用户到session中
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:00:55
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void setSessionAdminUser(User user) {
		setSession(GlobalConstant.ADMIN_TOKEN,user);
	}
	
	/**
	 * 
	 * 功能描述：保存变量到session中
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:01:22
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void setSession(String key,Object obj) {
		setSessionAttr(key,obj);
	}
	
	/**
	 * 
	 * 功能描述：清空所有session
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:02:06
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void invalidateSession() {
		getSession().invalidate();
	}
	
	/**
	 * 
	 * 功能描述：清空某一session
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:02:45
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void removeSession(String sessionName) {
		removeSessionAttr(sessionName);
	}
	
	/**
	 * 
	 * 功能描述：清空用户登录信息
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:04:10
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void removeSessionUser() {
		removeSession(GlobalConstant.USER_TOKEN);
	}
	
	protected void removeCookieUser() {
		removeCookie(GlobalConstant.USER_TOKEN);
	}
	
	/**
	 * 
	 * 功能描述：清空管理员登录信息
	 * 作        者：尹东东
	 * 创建时间：2013-1-5 下午11:04:14
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	protected void removeSessionAdminUser() {
		removeSession(GlobalConstant.ADMIN_TOKEN);
	}
	
	/**
	 * 
	 * 功能描述：扩展controller方法，让其支持多个model的自动绑定
	 * 作        者：尹东东
	 * 创建时间：2013-5-25 下午3:17:10
	 * 参       数:modelClass为model类；viewModel为表单中绑定数据的名称 如：user[0....n].[attr],user就为viewModel
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public <T> List<T> getModels(Class<T> modelClass,String viewModel){
		Pattern p = Pattern.compile(viewModel + "\\[\\d\\].[a-zA-z0-9]+");
		Map<String, String[]> parasMap = getRequest().getParameterMap();
		String paraKey;
		Set<String> modelPrefix = new HashSet<String>();
		for (Entry<String, String[]> e : parasMap.entrySet()) {
			paraKey = e.getKey();
			if(p.matcher(paraKey).find()){
				modelPrefix.add(paraKey.split("\\.")[0]);
			}
		}
		List<T> resultList = new ArrayList<T>();
		for (String modelName : modelPrefix) {
			resultList.add(getModel(modelClass,modelName));
		}
		return resultList;
	}
}
