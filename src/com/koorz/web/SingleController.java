/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.web;

import org.apache.commons.lang.StringUtils;

import com.jfinal.aop.Before;
import com.jfinal.plugin.ehcache.CacheInterceptor;
import com.koorz.interceptor.CommonsInterceptor;
import com.koorz.jfinal.route.ControllerBind;
import com.koorz.modules.image.Image;

/**
 * 功能描述：单品
 * 作        者：尹东东 
 * 创建时间：2013-6-2 下午3:20:41
 * 版  本  号：1.0
 */
@ControllerBind(controllerKey = "/single", viewPath = "/")
public class SingleController extends BaseController{
	
	/**
	 * 
	 * 功能描述：所有单品，当有参数时显示单品详情页
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午3:22:08
	 * 参       数:单品id
	 * U R L:/single/id
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void index() {
		String id = getPara();
		if(StringUtils.isEmpty(id)){
			setAttr("api", "/api/single");
			render("index.html");
		}else {
			Image image = Image.getSingle(id);
			if(image == null){
				renderError(404);
			}else {
				setAttr("image", image);
				render("detail/item.html");
			}
		}
	}
	
	/**
	 * 
	 * 功能描述：衣服
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午5:35:26
	 * 参       数:
	 * U R L:/single/dress 或 /single/dress/标签组  或 /single/dress/标签组 - 标签
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void dress(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isEmpty(lg)
				&& StringUtils.isEmpty(l)){
			setAttr("api", "/api/single/100000");
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			setAttr("api", "/api/single/100000-"+lg+"-"+l);
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isEmpty(l)){//标签组-0
			setAttr("api", "/api/single/100000-"+lg);
			render("index.html");
		}else if(StringUtils.isNotEmpty(l) && StringUtils.isEmpty(lg)){//0-标签
			setAttr("api", "/api/single/100000-0-"+l);
			render("index.html");
		}
	}
	
	/**
	 * 
	 * 功能描述：鞋子
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午5:35:50
	 * 参       数:
	 * U R L:/single/shoes 或 /single/shoes/标签组  或 /single/shoes/标签组 - 标签
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void shoes(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isEmpty(lg)
				&& StringUtils.isEmpty(l)){
			setAttr("api", "/api/single/100001");
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			setAttr("api", "/api/single/100001-"+lg+"-"+l);
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isEmpty(l)){//标签组-0
			setAttr("api", "/api/single/100001-"+lg);
			render("index.html");
		}else if(StringUtils.isNotEmpty(l) && StringUtils.isEmpty(lg)){//0-标签
			setAttr("api", "/api/single/100001-0-"+l);
			render("index.html");
		}
	}
	
	/**
	 * 
	 * 功能描述：包包
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午5:35:54
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void bags(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isEmpty(lg)
				&& StringUtils.isEmpty(l)){
			setAttr("api", "/api/single/100002");
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			setAttr("api", "/api/single/100002-"+lg+"-"+l);
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isEmpty(l)){//标签组-0
			setAttr("api", "/api/single/100002-"+lg);
			render("index.html");
		}else if(StringUtils.isNotEmpty(l) && StringUtils.isEmpty(lg)){//0-标签
			setAttr("api", "/api/single/100002-0-"+l);
			render("index.html");
		}
	}
	
	/**
	 * 
	 * 功能描述：配饰
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午5:35:58
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void access(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isEmpty(lg)
				&& StringUtils.isEmpty(l)){
			setAttr("api", "/api/single/100003");
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			setAttr("api", "/api/single/100003-"+lg+"-"+l);
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isEmpty(l)){//标签组-0
			setAttr("api", "/api/single/100003-"+lg);
			render("index.html");
		}else if(StringUtils.isNotEmpty(l) && StringUtils.isEmpty(lg)){//0-标签
			setAttr("api", "/api/single/100003-0-"+l);
			render("index.html");
		}
	}
	
	/**
	 * 
	 * 功能描述：美妆
	 * 作        者：尹东东
	 * 创建时间：2013-6-2 下午5:36:02
	 * 参       数:
	 * U R L:
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({CommonsInterceptor.class,CacheInterceptor.class})
	public void beauty(){
		String lg = getPara(0);
		String l = getPara(1);
		if(StringUtils.isEmpty(lg)
				&& StringUtils.isEmpty(l)){
			setAttr("api", "/api/single/100004");
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isNotEmpty(l)){ //标签组-标签
			setAttr("api", "/api/single/100004-"+lg+"-"+l);
			render("index.html");
		}else if(StringUtils.isNotEmpty(lg) && StringUtils.isEmpty(l)){//标签组-0
			setAttr("api", "/api/single/100004-"+lg);
			render("index.html");
		}else if(StringUtils.isNotEmpty(l) && StringUtils.isEmpty(lg)){//0-标签
			setAttr("api", "/api/single/100004-0-"+l);
			render("index.html");
		}
	}
}
