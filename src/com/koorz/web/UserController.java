/**
 * http://www.koorz.com
 * Copyright (c) 2012 shanghai meiku information technology co,.ltd
 */
package com.koorz.web;

import org.apache.commons.lang.StringUtils;

import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.token.TokenManager;
import com.koorz.interceptor.AuthInterceptor;
import com.koorz.interceptor.CommonsInterceptor;
import com.koorz.jfinal.route.ControllerBind;
import com.koorz.modules.commission.DrawRecord;
import com.koorz.modules.user.User;
import com.koorz.oauth.model.Verifier;
import com.koorz.oauth.oauth.OAuthService;
import com.koorz.oauth.services.DoubanService;
import com.koorz.oauth.services.RenrenService;
import com.koorz.oauth.services.TaobaoService;
import com.koorz.oauth.services.TencentService;
import com.koorz.oauth.services.WeiboService;
import com.koorz.utils.ActionUtils;
import com.koorz.utils.LoginUtil;
import com.koorz.utils.LoginUtil.WeiboType;
import com.koorz.utils.SessionContext;
import com.koorz.utils.SystemConfig;

/**
 * 功能描述：用户登录、退出、设置、消息、约请好友、收藏、分享、收听
 * 作        者：尹东东 
 * 创建时间：2013-1-24 下午3:10:09
 * 版  本  号：1.0
 */
@ControllerBind(controllerKey="/user",viewPath="")
public class UserController extends BaseController{
	
	/**
	 * 
	 * 功能描述：用户首页
	 * 作        者：尹东东
	 * 创建时间：2013-1-24 下午3:59:59
	 * 参       数：用户id
	 * URL  :/user/xxx
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(CommonsInterceptor.class)
	public void index(){
		String id = getPara();
		if(StringUtils.isEmpty(id)){
			redirect("/",false);
		}else {
			User user = SessionContext.get().user();
			if(user == null || (user != null && !user.getStr("id").equals(id))){
				user = User.getUser("id", id);
				if(user == null)renderError(404);
				else setAttr("user", user);
			}else {
				setAttr("user", user);
			}
			setAttr("api", "/api/user/"+id);
			render("index.html");
		}
	}

	/**
	 * 
	 * 功能描述：第三方账号登录入口
	 * 作        者：尹东东
	 * 创建时间：2013-1-17 下午7:06:34
	 * 参       数：snsType
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void snsLogin(){
		SessionContext context = SessionContext.get();
		if (context.user() != null) {
			redirect("/", false);
		} else {
			int snsType = getParaToInt("snsType");
			if(LoginUtil.hasType(snsType))redirect(LoginUtil.getInstance().getOAuthService(snsType).getAuthorizationUrl());
			else renderError(404);
		}
	}
	
	/**
	 * 
	 * 功能描述：回调地址
	 * 作        者：尹东东
	 * 创建时间：2013-1-17 下午7:06:40
	 * 参       数：
	 * U R L:/user/snsBind
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void snsBind(){
		String state = getPara("state");
		if(LoginUtil.validate(state)){
			int snsType = LoginUtil.getState(state);
			OAuthService os = LoginUtil.getInstance().getOAuthService(snsType);
			User user = null;
			if(snsType == WeiboType.SINA.ordinal()){
				WeiboService weiboService = new WeiboService();
				user = weiboService.bindUser(os.getAccessToken(new Verifier(getPara("code"))));
			}else if(snsType == WeiboType.DOUBAN.ordinal()){
				DoubanService weiboService = new DoubanService();
				user = weiboService.bindUser(os.getAccessToken(new Verifier(getPara("code"))));
			}else if(snsType == WeiboType.TERCENT.ordinal()){
				TencentService weiboService = new TencentService();
				user = weiboService.bindUser(os.getAccessToken(new Verifier(getPara("code"))));
			}else if(snsType == WeiboType.RENREN.ordinal()){
				RenrenService weiboService = new RenrenService();
				user = weiboService.bindUser(os.getAccessToken(new Verifier(getPara("code"))));
			}else if(snsType == WeiboType.QQSPACE.ordinal()){
				//QQ空间
				
			}else if(snsType == WeiboType.TAOBAO.ordinal()){
				//淘宝
				TaobaoService weiboService = new TaobaoService();
				user = weiboService.bindUser(os.getAccessToken(new Verifier(getPara("code"))));
			}
			SessionContext context = SessionContext.get();
			context.saveUserInCookie(user, true);
			setAttr("code", ActionUtils.CODE_200);
			render("redirect.html");
		}else {
			renderError(404);
		}
	}
	
	/**
	 * 
	 * 功能描述：退出
	 * 作        者：尹东东
	 * 创建时间：2013-1-17 下午7:06:45
	 * 参       数：
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void logout(){
		SessionContext context = SessionContext.get();
		context.deleteUserInCookie();
		redirect("/", false);
	}
	
	/**
	 * 
	 * 功能描述：我的资产页面
	 * 作        者：尹东东
	 * 创建时间：2013-6-18 下午9:48:04
	 * 参       数:
	 * U R L:/user/assets
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({AuthInterceptor.class,CommonsInterceptor.class})
	public void assets(){
		render("assets.html");
	}
	
	/**
	 * 
	 * 功能描述：订单记录
	 * 作        者：尹东东
	 * 创建时间：2013-6-20 上午11:26:36
	 * 参       数:
	 * U R L:/user/orderRecord
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(AuthInterceptor.class)
	public void orderRecord(){ 
		render("order_record.html");
	}
	
	/**
	 * 
	 * 功能描述：奖励记录
	 * 作        者：尹东东
	 * 创建时间：2013-6-20 上午11:27:06
	 * 参       数:
	 * U R L:/user/rewardRecord
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(AuthInterceptor.class)
	public void rewardRecord(){ 
		render("reward_record.html");
	}
	
	/**
	 * 
	 * 功能描述：提现记录
	 * 作        者：尹东东
	 * 创建时间：2013-6-20 上午11:27:29
	 * 参       数:
	 * U R L:/user/drawRecord
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(AuthInterceptor.class)
	public void drawRecord(){ 
		render("draw_record.html");
	}
	
	/**
	 * 
	 * 功能描述：提现设置
	 * 作        者：尹东东
	 * 创建时间：2013-6-20 上午11:16:27
	 * 参       数:
	 * U R L:/user/drawSetting
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(AuthInterceptor.class)
	public void drawSetting(){
		TokenManager.createToken(this, tokenName, 0);
		render("draw_setting.html");
	}
	
	/**
	 * 
	 * 功能描述：保存提现设置
	 * 作        者：尹东东
	 * 创建时间：2013-6-18 下午10:00:51
	 * 参       数:
	 * U R L:/user/saveDrawSetting
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({AuthInterceptor.class,POST.class})
	public void saveDrawSetting(){
		if(TokenManager.validateToken(this, tokenName)){
			User currentUser = SessionContext.get().user();
			String alipay = getPara("alipay");
			String qq = getPara("qq");
			String realname = getPara("realname");
			if(StringUtils.isNotEmpty(alipay) && StringUtils.isNotEmpty(qq) && StringUtils.isNotEmpty(realname)){
				if(StringUtils.isEmpty(currentUser.getStr("alipay"))){
					currentUser.set("alipay", getPara("alipay"));
					currentUser.set("qq", getPara("qq"));
					currentUser.set("realname", getPara("realname"));
					currentUser.update();
					renderJson(ActionUtils.result(ActionUtils.CODE_200, "设置成功"));
				}else {
					renderJson(ActionUtils.result(ActionUtils.CODE_100, "已经设置过了，不能重复设置"));
				}
			}else {
				renderJson(ActionUtils.result(ActionUtils.CODE_100, "参数不正确"));
			}
		}else {
			renderJson(ActionUtils.result(ActionUtils.CODE_100, "不可以重复提交"));
		}
	}
	
	/**
	 * 
	 * 功能描述：提现申请
	 * 作        者：尹东东
	 * 创建时间：2013-6-20 上午11:28:13
	 * 参       数:
	 * U R L:/user/drawApply
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before(AuthInterceptor.class)
	public void drawApply(){
		TokenManager.createToken(this, tokenName, 0);
		render("draw_apply.html");
	}
	
	/**
	 * 
	 * 功能描述：提交提现申请
	 * 作        者：尹东东
	 * 创建时间：2013-6-18 下午10:00:56
	 * 参       数:
	 * U R L:/user/saveDrawApply
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	@Before({AuthInterceptor.class,POST.class})
	public void saveDrawApply(){
		if(TokenManager.validateToken(this, tokenName)){
			User currentUser = SessionContext.get().user();
			int money = getParaToInt("money");
			if(currentUser.getDouble("account_balance")  > money && StringUtils.isNotEmpty(currentUser.getStr("alipay"))){
				//更新用户余额
				User.updateBalance(currentUser, -money);
				
				//计算手续费
				double charge = money*SystemConfig.getToDouble("commission.charge_rate", 0.0);
				
				//生成提现申请
				DrawRecord record = new DrawRecord();
				record.set("alipay", currentUser.getStr("alipay"));
				record.set("money", money);
				record.set("commission_charge", charge);
				record.set("payed", money-charge);
				record.set("check_state", DrawRecord.Status.NOT_CHECK.ordinal());
				record.set("user_id", currentUser.getStr("id"));
				DrawRecord.saveDrawRecord(record);
				renderJson(ActionUtils.result(ActionUtils.CODE_200, "提交成功"));
			}else {
				renderJson(ActionUtils.result(ActionUtils.CODE_100, "余额不足"));
			}
		}else {
			renderJson(ActionUtils.result(ActionUtils.CODE_100, "不可以重复提交"));
		}
	}
	
	/**
	 * 
	 * 功能描述：用户发布的主题搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-21 上午11:44:58
	 * 参       数:
	 * U R L:/user/theme
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void theme(){
		
	}
	
	/**
	 * 
	 * 功能描述：用户发布的单品组合搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-21 上午11:45:18
	 * 参       数:
	 * U R L:/user/single
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void single(){
			
	}
	
	/**
	 * 
	 * 功能描述：用户发布的达人搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-21 上午11:45:40
	 * 参       数:
	 * U R L:/user/daren
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void daren(){
		
	}
	
	/**
	 * 
	 * 功能描述：用户发布的明星搭配
	 * 作        者：尹东东
	 * 创建时间：2013-6-21 上午11:45:57
	 * 参       数:
	 * U R L:/user/star
	 * 返       回:无
	 * 异       常：无
	 * 版  本 号：1.0
	 */
	public void star(){
		
	}
}